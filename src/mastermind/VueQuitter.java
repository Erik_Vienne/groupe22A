package mastermind;

import javafx.animation.KeyValue;
import javafx.application.Platform;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;

class VueQuitter {

    private BorderPane panel;

    VueQuitter() {
        this.setMenu();
    }

    Parent getScene() {
        return this.panel;
    }

    private void setMenu() {
        Platform.exit();
    }

}
