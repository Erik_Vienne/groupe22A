package mastermind;

public class ModeleJoueurM {
    private String pseudo;
    private ModeleCombinaison combiEnCours;

    ModeleJoueurM(String pseudo){
        this.combiEnCours = new ModeleCombinaison("0","0","0","0"); // Initialisation de la combinaison à 0000, ce qui ne représente aucune couleur
        this.pseudo = pseudo;
    }

    // Getters
    String getPseudo() {
        return pseudo;
    }

    ModeleCombinaison getCombiEnCours() {
        return combiEnCours;
    }

    // Change la combinaison actuelle
    void setCombi(ModeleCombinaison cb) {
        this.combiEnCours = cb;
    }

    @Override
    public String toString() {
        return pseudo;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null) return false;
        else {
            ModeleJoueurM jm = (ModeleJoueurM) o;
            return (this.pseudo.equals(jm.pseudo));
        }
    }
}
