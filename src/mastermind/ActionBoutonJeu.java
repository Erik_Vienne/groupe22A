package mastermind;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

class ActionBoutonJeu implements EventHandler<ActionEvent> {

    private VueApplication app;
    private VueJeuSolo jeu;

    ActionBoutonJeu(VueApplication app, VueJeuSolo jeu) {
        this.app = app;
        this.jeu = jeu;
    }

    @Override
    public void handle(ActionEvent event) {
        Button but = (Button) event.getTarget();
        switch (but.getText()) {
            case("Essayer"): this.jeu.setEss(true); this.jeu.appuiEssai(); break;
            case("Manche\nSuivante"): this.jeu.reset(); break;
            case("Rejouer"): this.app.setJeuSolo(); break;
            case("Menu"): this.app.setMenu(); break;
        }
    }
}
