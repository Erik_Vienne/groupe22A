package mastermind;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.util.Duration;
import java.util.ArrayList;
import java.util.List;

class VueJeuSolo {

    private int width, height;
    private ModeleMastermind mt;
    private ModeleJoueurM joueur;
    private ModeleChronometre chronometre;
    private VueGrille grille;
    private HBox panel;
    private Button ess;
    private Button quit;
    private ComboBox<String> un,deux,trois,quatre;
    private ArrayList<Circle> lstCercle;
    private Label console;
    private Timeline tl;

    private Label manche;
    private Label chrono;
    private Label essais;
    private Label score;
    private Label scoreT;

    private VueApplication app;


    VueJeuSolo(VueApplication app, int widht, int height) {
        this.joueur = new ModeleJoueurM("xXD4RkS4sUkEXx");
        this.mt = new ModeleMastermind(joueur);
        this.chronometre = this.mt.getChrono();
        this.app = app;
        this.height = height;
        this.width = widht;
        this.tl = new Timeline();
        this.setJeu();
    }


    Parent getScene() {
        return this.panel;
    }

    private void setJeu() {

        // Instanciation des handler
        ActionComboJeu actComb = new ActionComboJeu(this.app, this);
        ActionBoutonJeu actOpt = new ActionBoutonJeu(this.app, this);

        VBox affichages = new VBox();
        affichages.setSpacing(5);
        affichages.setAlignment(Pos.TOP_LEFT);

        // Création des affichages à gauche
        List<Label> lst = new ArrayList<>();
        Label adversaire = new Label("Contre l'ordinateur");
        lst.add(adversaire);
        this.manche = new Label("Manche n°" + mt.getNumManche());
        lst.add(manche);
        this.chrono = new Label("00:00:00");
        lst.add(chrono);
        this.essais = new Label("Essais : " + mt.getNbEssais());
        lst.add(essais);
        this.score = new Label("Score : 1000");
        lst.add(score);
        this.scoreT = new Label("Score total : " + mt.getNbPointsTotal());
        lst.add(scoreT);

        VBox.setMargin(adversaire,new Insets(20,20,15,20));
        VBox.setMargin(manche,new Insets(15,20,15,20));
        VBox.setMargin(chrono,new Insets(35,20,5,20));
        VBox.setMargin(essais,new Insets(5,20,5,20));
        VBox.setMargin(score,new Insets(5,20,5,20));
        VBox.setMargin(scoreT,new Insets(5,20,30,20));

        this.tl = new Timeline();
        this.tl.setCycleCount(Timeline.INDEFINITE);
        ActionTemps at = new ActionTemps(this);
        this.tl.getKeyFrames().add(new KeyFrame(Duration.millis(10), at));


        for (Label l : lst){
            l.setTextFill(Info.NORMCOLOR.get(Info.APPCOLOR));
            l.setFont(Font.font(20));
            l.setId("text");
            l.setPrefWidth(Info.WIDTH / 4);
            l.setPadding(new Insets(10,0,10,0));
            l.setAlignment(Pos.CENTER);
            affichages.getChildren().add(l);
        }

        this.console = new Label("");
        this.console.setPrefWidth(Info.WIDTH / 4);
        this.console.setPrefHeight(Info.HEIHT / 3);
        this.console.setId("text");
        this.console.setAlignment(Pos.TOP_LEFT);
        this.console.setPadding(new Insets(20,20,5,20));
        this.console.setText("Entrez un combinaison pour commencer la\nmanche");
        VBox.setMargin(this.console,new Insets(0,20,0,20));
        affichages.getChildren().add(this.console);

        //Création du bas du jeu
        // Création du bouton Essayer
        HBox bas = new HBox();
        bas.setAlignment(Pos.CENTER);
        bas.setPadding(new Insets(0,0,10,this.width/45));
        bas.setSpacing(25);

        // Création des pions de la combinaison
        this.lstCercle = new ArrayList<>();

        this.lstCercle.add( new Circle(this.width/80,Color.rgb(0,0,180)));
        this.lstCercle.add( new Circle(this.width/80,Color.rgb(0,0,180)));
        this.lstCercle.add( new Circle(this.width/80,Color.rgb(0,0,180)));
        this.lstCercle.add( new Circle(this.width/80,Color.rgb(0,0,180)));

        // Création des Combos Box
        this.un = new ComboBox<>();
        this.deux = new ComboBox<>();
        this.trois = new ComboBox<>();
        this.quatre = new ComboBox<>();
        List<ComboBox<String>> list = new ArrayList<>();
        list.add(un);
        list.add(deux);
        list.add(trois);
        list.add(quatre);
        for (ComboBox<String> cb : list){
            cb.getItems().add("Violet");
            cb.getItems().add("Blanc");
            cb.getItems().add("Vert");
            cb.getItems().add("Jaune");
            cb.getItems().add("Rouge");
            cb.getItems().add("Bleu");
            cb.setValue("Bleu");
            cb.setPrefWidth(100);
            cb.setId("pion");
            cb.setOnAction(actComb);
            cb.setOnMouseEntered(this.app.getHover());
        }

        for(int i = 0; i < 4; i++) {
            VBox pan = new VBox();
            pan.setAlignment(Pos.CENTER);
            pan.setSpacing(15);
            pan.getChildren().add(this.lstCercle.get(i));
            pan.getChildren().add(list.get(i));
            bas.getChildren().add(pan);
        }

        // Création des boutons du bas
        this.ess = new Button("Essayer");
        this.ess.setPrefSize(130,50);
        this.ess.setFont(Font.font(15));
        this.ess.setOnAction(actOpt);
        this.ess.setOnMouseEntered(this.app.getHover());
        this.ess.setTextAlignment(TextAlignment.CENTER);

        this.quit = new Button("Menu");
        this.quit.setPrefSize(130,50);
        this.quit.setFont(Font.font(20));
        this.quit.setOnAction(actOpt);
        this.quit.setOnMouseEntered(this.app.getHover());
        this.quit.setVisible(false);
        this.quit.setTextAlignment(TextAlignment.CENTER);

        bas.getChildren().add(this.ess);
        bas.getChildren().add(this.quit);

        // Création de la grille de mastermind
        this.grille = new VueGrille(this.app, Info.WIDTH, Info.HEIHT);

        // Ajout des boutons dans le corps de la fenètre
        BorderPane corp = new BorderPane();
        BorderPane.setAlignment(bas,Pos.CENTER_LEFT);
        BorderPane.setAlignment(this.grille.getScene(),Pos.CENTER_LEFT);

        corp.setBottom(bas);
        corp.setCenter(this.grille.getScene());

        // Création du conteneur final
        this.panel = new HBox();
        this.panel.setSpacing(this.width/15);
        this.panel.getChildren().add(affichages);
        this.panel.getChildren().add(corp);
    }

    void updateInfo() {
        this.chrono.setText(this.chronometre.getTempsAffich());
        this.score.setText("Score : " + this.mt.getNbPoints());
        this.essais.setText("Essais : " + this.mt.getNbEssais());
    }

    void updatePions() {
        this.lstCercle.get(0).setFill(coulToPaint(this.un));
        this.lstCercle.get(1).setFill(coulToPaint(this.deux));
        this.lstCercle.get(2).setFill(coulToPaint(this.trois));
        this.lstCercle.get(3).setFill(coulToPaint(this.quatre));
    }

    void setEss(boolean state) {
        this.ess.disableProperty().setValue(state);
    }

    private String coulToNum(ComboBox<String> cb) {
        String num = "";
        switch(cb.getValue()) {
            case("Violet"): num = "1"; break;
            case("Blanc"): num = "2"; break;
            case("Vert"): num = "3"; break;
            case("Jaune"): num = "4";break;
            case("Rouge"): num = "5"; break;
            case("Bleu"): num = "6"; break;
        }
        return num;
    }

    private Color coulToPaint(ComboBox<String> cb) {
        Color res = Color.rgb(0,0,0);
        switch(cb.getValue()) {
            case("Violet"): res = Color.rgb(170,0,150); break;
            case("Blanc"): res = Color.rgb(255,255,255); break;
            case("Vert"): res = Color.rgb(0,170,0); break;
            case("Jaune"): res = Color.rgb(200,200,0); break;
            case("Rouge"): res = Color.rgb(180,0,0); break;
            case("Bleu"): res = Color.rgb(0,0,180); break;
        }
        return res;
    }

    void appuiEssai(){

        if(!chronometre.isStart()){
            chronometre.start();
            this.tl.play();
            this.console.setText("");
        }

        List<String> lst = new ArrayList<>();
        lst.add(this.coulToNum(this.un));
        lst.add(this.coulToNum(this.deux));
        lst.add(this.coulToNum(this.trois));
        lst.add(this.coulToNum(this.quatre));
        this.joueur.setCombi(new ModeleCombinaison(lst));

        System.out.println(mt.getCombi());

        this.grille.insererLigne(this.joueur.getCombiEnCours().getListe(), this.mt.getCombi().getNbBon(this.joueur.getCombiEnCours()),
                this.mt.getCombi().getNbMau(this.joueur.getCombiEnCours()));

        if (this.mt.essaiCombi()) {

            this.mt.mancheFinie();

            if(this.mt.isPartieFinie()) {
                this.essais.setText("Essais : " + this.mt.getNbEssais());
                String txt = this.console.getText();
                String res = "";
                res += "Try " + this.coulToNum(this.un) + " - " + this.coulToNum(this.deux) + " - " + this.coulToNum(this.trois) + " - " + this.coulToNum(this.quatre);
                res += "\nConnecting...\nSuccess!";
                res += "\nBravo, vous avez trouvé la combinaison" + "\n\n";
                res += "Fin de partie, score final : "+this.mt.getNbPointsTotal()+"\n";
                res += "Vous pouvez rejouer ou retourner au menu";
                this.console.setText(res + txt);
            } else {
                this.essais.setText("Essais : " + this.mt.getNbEssais());
                String txt = this.console.getText();
                String res = "";
                res += "Try " + this.coulToNum(this.un) + " - " + this.coulToNum(this.deux) + " - " + this.coulToNum(this.trois) + " - " + this.coulToNum(this.quatre);
                res += "\nConnecting...\nSuccess!";
                res += "\nBravo, vous avez trouvé la combinaison" + "\n\n";
                this.console.setText(res + txt);
                this.changerManche();
            }
        }

        else{
            String txt = this.console.getText();
            String res = "";
            res+="Try "+this.coulToNum(this.un)+" - "+this.coulToNum(this.deux)+" - "+this.coulToNum(this.trois)+" - "+this.coulToNum(this.quatre);
            res+="\nConnecting...\nFailed!";
            res+="\n    Nbr pions bien placé : "+String.valueOf( this.mt.getCombi().getNbBon(this.joueur.getCombiEnCours()) );
            res+="\n    Nbr pions mal placé : "+String.valueOf( this.mt.getCombi().getNbMau(this.joueur.getCombiEnCours()) )+"\n\n";
            this.console.setText(res+txt);
         }
    }

    private void changerManche() {
        if(this.mt.isPartieFinie()) {
            this.finMatch();
        } else {
            this.tl.stop();
            this.ess.setDisable(false);
            this.ess.setText("Manche\nSuivante");
            this.scoreT.setText("Score total : " + this.mt.getNbPointsTotal());
            this.chronometre = this.mt.getChrono();
        }
    }

    void reset() {
        this.manche.setText("Manche n°"+this.mt.getNumManche());
        this.score.setText("1000");
        this.chrono.setText("00:00:00");
        this.console.setText("Entrez un combinaison pour commencer la\nmanche");
        this.ess.setText("Essayer");
        this.essais.setText("Essais : 0");
        this.grille.reset();

    }

    private void finMatch() {
        this.ess.setText("Rejouer");
        this.quit.setVisible(true);
    }
}
