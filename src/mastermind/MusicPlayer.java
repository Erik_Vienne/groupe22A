package mastermind;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

class MusicPlayer {

    private MediaPlayer click;
    private MediaPlayer hover;
    private MediaPlayer menu;

    MusicPlayer() {
        this.click = new MediaPlayer( new Media( new File(Info.FILEPATH+"snd/click.wav").toURI().toString()));
        this.click.setVolume(Info.VOLSOUNDS);
        this.click.setCycleCount(1);
        this.hover = new MediaPlayer( new Media( new File(Info.FILEPATH+"snd/hover.wav").toURI().toString()));
        this.hover.setVolume(Info.VOLSOUNDS);
        this.hover.setCycleCount(1);
        this.menu = new MediaPlayer( new Media( new File(Info.FILEPATH+"snd/menu.wav").toURI().toString()));
        this.menu.setVolume(Info.VOLMUS);
        this.menu.setCycleCount(MediaPlayer.INDEFINITE);
    }

    void playClick() {
        this.click.stop();
        this.click.play();
    }

    void playHover() {
        this.hover.stop();
        this.hover.play();
    }

    void playMenu() {
        this.menu.stop();
        this.menu.play();
    }

    void updateMus() {
        this.menu.setVolume(Info.VOLMUS);
    }

    void updateSnd() {
        this.click.setVolume(Info.VOLSOUNDS);
        this.hover.setVolume(Info.VOLSOUNDS);
    }
}
