package mastermind;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.io.File;

class VueOptions {

    private int width;
    private int height;
    private BorderPane panel;
    private Button appli;
    private ComboBox<String> resCB;

    private VueApplication app;

    VueOptions(VueApplication app, int widht, int height) {
        this.app = app;
        this.height = height;
        this.width = widht;
        this.setOptions();
    }

    Parent getScene() {
        return this.panel;
    }

    private void setOptions() {

        // Instanciation des handler
        ActionCheckboxOptions actChk = new ActionCheckboxOptions(this.app,this);
        ActionBoutonOptions actOpt = new ActionBoutonOptions(this.app);
        ActionComboOptions actComb = new ActionComboOptions(this.app, this);


        // Création du titre
        FlowPane titre = new FlowPane();
        titre.setPadding(new Insets(this.height/8,0,0,this.width/45));

        Image titreImage = new Image(new File(Info.FILEPATH+"img/parametres"+Info.APPCOLOR+".png").toURI().toString());
        ImageView titreAff = new ImageView();
        titreAff.setImage(titreImage);
        titreAff.setSmooth(true);

        titre.getChildren().add(titreAff);


        // Création des réglages
        VBox boutons = new VBox();
        boutons.setPadding(new Insets(0,0,0,this.width/45));
        boutons.setAlignment(Pos.CENTER_LEFT);
        boutons.setSpacing(60);
        VBox video = new VBox();
        video.setSpacing(5);
        VBox audio = new VBox();
        audio.setSpacing(5);

        // Création des boutons de réglages vidéo
        // Textes
        Text resText = new Text("Résolution");
        resText.setFill(Info.NORMCOLOR.get(Info.APPCOLOR));
        Label colText = new Label("Couleur de l'interface");
        colText.setTextFill(Info.NORMCOLOR.get(Info.APPCOLOR));

        // ComboBox
        this.resCB = new ComboBox<>();
        if(!"1960x10801280x7201280x1024800x600".contains((Info.SCRWIDTH)+"x"+String.valueOf(Info.SCRHEIGHT)))
            this.resCB.getItems().add(String.valueOf(Info.SCRWIDTH)+"x"+String.valueOf(Info.SCRHEIGHT));
        this.resCB.getItems().add("1960x1080");
        this.resCB.getItems().add("1280x720");
        this.resCB.getItems().add("1280x1024");
        this.resCB.getItems().add("800x600");
        this.resCB.setValue(String.valueOf(Info.WIDTH)+"x"+String.valueOf(Info.HEIHT));
        this.resCB.setPrefWidth(180);
        this.resCB.setId("res");
        this.resCB.setDisable(Info.FULLSCREEN);
        this.resCB.setOnAction(actComb);
        this.resCB.setOnMouseEntered(this.app.getHover());

        ComboBox<String> colCB = new ComboBox<>();
        colCB.getItems().add("Vert");
        colCB.getItems().add("Bleu");
        colCB.getItems().add("Orange");
        colCB.setValue(Info.APPCOLOR);
        colCB.setPrefWidth(180);
        colCB.setId("col");
        colCB.setOnAction(actComb);
        colCB.setOnMouseEntered(this.app.getHover());

        // CheckBox
        CheckBox fullCheck = new CheckBox("Plein écran");
        fullCheck.setSelected(Info.FULLSCREEN);
        fullCheck.setOnAction(actChk);

        // Ajout de tous les éléments à video
        video.getChildren().add(resText);
        video.getChildren().add(this.resCB);
        video.getChildren().add(colText);
        video.getChildren().add(colCB);
        video.getChildren().add(fullCheck);

        VBox.setMargin(colText,new Insets(12,0,0,0));
        VBox.setMargin(fullCheck, new Insets(12,0,0,0));


        // Création des réglages audio
        // Textes
        Text musText = new Text("Volume de la musique");
        musText.setFill(Info.NORMCOLOR.get(Info.APPCOLOR));
        Text sonText = new Text("Volume des sons");
        sonText.setFill(Info.NORMCOLOR.get(Info.APPCOLOR));

        // Sliders
        Slider musSli = new Slider(0,1,1.0);
        musSli.setPadding(new Insets(0,this.width/1.2,0,0));
        musSli.setValue(Info.VOLMUS);
        musSli.valueProperty().addListener(new ActionSliderOptions(this.app, this, "mus"));

        Slider sonSli = new Slider(0,1,1);
        sonSli.setPadding(new Insets(0,this.width/1.2,0,0));
        sonSli.setValue(Info.VOLSOUNDS);
        sonSli.valueProperty().addListener(new ActionSliderOptions(this.app, this, "snd"));

        // CheckBox
        CheckBox chatSoundCheck = new CheckBox("Sons du Chat");
        chatSoundCheck.setSelected(Info.CHATSOUNDS);
        chatSoundCheck.setOnAction(actChk);

        // Ajout de tous les éléments à audio
        audio.getChildren().add(musText);
        audio.getChildren().add(musSli);
        audio.getChildren().add(sonText);
        audio.getChildren().add(sonSli);
        audio.getChildren().add(chatSoundCheck);

        VBox.setMargin(sonText, new Insets(12,0,0,0));
        VBox.setMargin(chatSoundCheck, new Insets(12,0,0,0));

        // Rassemblement des réglages videos et audios
        boutons.getChildren().add(video);
        boutons.getChildren().add(audio);


        // Création des boutons du bas
        HBox bas = new HBox();
        bas.setSpacing(10);
        bas.setPadding(new Insets(0,0,this.height/30,this.width/45));

        Button ret = new Button("Retour");
        ret.setPrefSize(250,40);
        ret.setOnAction(actOpt);
        ret.setOnMouseEntered(this.app.getHover());

        this.appli = new Button("Appliquer");
        this.appli.setPrefSize(250,40);
        this.appli.setOnAction(actOpt);
        this.appli.setOnMouseEntered(this.app.getHover());
        this.appli.disableProperty().setValue(true);

        bas.getChildren().add(ret);
        bas.getChildren().add(appli);

        // Création du conteneur final
        this.panel = new BorderPane();
        this.panel.setTop(titre);
        this.panel.setCenter(boutons);
        this.panel.setBottom(bas);
    }

    void updateResCB(Boolean state) {
        this.resCB.disableProperty().setValue(state);
        this.resCB.setValue(String.valueOf(Info.WIDTH)+"x"+String.valueOf(Info.HEIHT));
    }

    void unableAppli() {
        this.appli.disableProperty().setValue(false);
    }

}



class ActionBoutonOptions implements EventHandler<ActionEvent> {

    private VueApplication app;

    ActionBoutonOptions(VueApplication app) {
        this.app = app;
    }

    @Override
    public void handle(ActionEvent event) {
        this.app.clickSound();
        Button but = (Button) event.getTarget();
        switch (but.getText()) {
            case("Retour"): this.app.setMenu(); Info.load(); break;
            case("Appliquer"): Info.save(); this.app.reload(); but.disableProperty().setValue(true); break;
        }
    }
}



class ActionCheckboxOptions implements EventHandler<ActionEvent> {

    private VueApplication app;
    private VueOptions vueOpt;

    ActionCheckboxOptions(VueApplication app, VueOptions vueOpt) {
        this.app = app;
        this.vueOpt = vueOpt;
    }

    @Override
    public void handle(ActionEvent event) {
        this.app.clickSound();
        CheckBox chk = (CheckBox) event.getTarget();
        switch (chk.getText()) {
            case("Plein écran"): Info.FULLSCREEN = chk.isSelected(); Info.WIDTH = Info.SCRWIDTH; Info.HEIHT = Info.SCRHEIGHT;
            this.vueOpt.updateResCB(chk.isSelected()); break;
            case("Sons du Chat"): Info.CHATSOUNDS = chk.isSelected(); break;
        }
        this.vueOpt.unableAppli();
    }
}



class ActionComboOptions implements EventHandler<ActionEvent> {

    private VueApplication app;
    private VueOptions vueOpt;

    ActionComboOptions(VueApplication app, VueOptions vueOpt) {
        this.app = app;
        this.vueOpt = vueOpt;
    }

    @Override
    public void handle(ActionEvent event) {
        this.app.clickSound();
        ComboBox<String> comb = (ComboBox<String>) event.getTarget();
        if(comb.getId().equals("res")) {
            String[] dim = comb.getValue().split("x");
            Info.WIDTH = Integer.valueOf(dim[0]);
            Info.HEIHT = Integer.valueOf(dim[1]);
        } else {
            Info.APPCOLOR = comb.getValue();
            this.vueOpt.unableAppli();
        }
        this.vueOpt.unableAppli();
    }
}



class ActionSliderOptions implements ChangeListener<Number> {

    private VueApplication app;
    private VueOptions vueOpt;
    private String name;

    ActionSliderOptions(VueApplication app, VueOptions vueOpt, String name) {
        this.app = app;
        this.vueOpt = vueOpt;
        this.name = name;
    }

    @Override
    public void changed(ObservableValue<? extends Number> ov, Number old_val, Number new_val) {
        switch (this.name) {
            case("mus"): Info.VOLMUS = new_val.doubleValue(); this.app.updateMusVol(); break;
            case("snd"): Info.VOLSOUNDS = new_val.doubleValue(); this.app.updateSndVol();break;
        }
    }
}
