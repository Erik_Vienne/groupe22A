package mastermind;

import javafx.geometry.Rectangle2D;
import javafx.scene.paint.Color;
import javafx.stage.Screen;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

abstract class Info {

    // Classe pour les paramètres de l'application

    private static Rectangle2D screen = Screen.getPrimary().getBounds();
    static final int SCRWIDTH = (int) screen.getWidth();
    static final int SCRHEIGHT = (int) screen.getHeight();

    static int WIDTH = (int) screen.getWidth();
    static int HEIHT = (int) screen.getHeight();

    static String APPCOLOR = "Vert";

    static Map<String, Color> NORMCOLOR = new HashMap<>();
    static Map<String, Color> DARKCOLOR = new HashMap<>();

    static boolean FULLSCREEN = true;

    static boolean CHATSOUNDS = true;

    static double VOLSOUNDS = 1.0;
    static double VOLMUS = 1.0;

    // Important à changer pour le build!
    static String FILEPATH = "src/mastermind/";

    static void load() {
        try {
            FileReader fr = new FileReader(new File(".config"));
            BufferedReader br = new BufferedReader(fr);
            Info.APPCOLOR = br.readLine();
            Info.FULLSCREEN = Boolean.valueOf(br.readLine());
            Info.CHATSOUNDS = Boolean.valueOf(br.readLine());
            Info.WIDTH = Integer.valueOf(br.readLine());
            if(Info.WIDTH > Info.SCRWIDTH) Info.WIDTH = Info.SCRWIDTH;
            Info.HEIHT = Integer.valueOf(br.readLine());
            if(Info.HEIHT > Info.SCRHEIGHT) Info.HEIHT = Info.SCRHEIGHT;
            Info.VOLSOUNDS = Double.valueOf(br.readLine());
            Info.VOLMUS = Double.valueOf(br.readLine());

        } catch(Exception e) {
            Info.save();
        }
    }

    static void save() {
        try {
            FileWriter fw = new FileWriter(".config", false);
            BufferedWriter bw = new BufferedWriter(fw);
            String res = Info.APPCOLOR + "\n" + String.valueOf(Info.FULLSCREEN) + "\n" + String.valueOf(Info.CHATSOUNDS) + "\n"
                    + String.valueOf(Info.WIDTH) + "\n" + String.valueOf(Info.HEIHT) + "\n" + String.valueOf(Info.VOLSOUNDS)
                    + "\n" + Info.VOLMUS;
            bw.write(res);
            bw.close();
        }
        catch (IOException e)
        {
            System.out.println("Erreur de sauvegarde des paramètres: " + e.getMessage());
        }
    }

}
