package mastermind;

public class ModeleChronometre{
    private long chrono, chrono2, dureeMls;
    private boolean start;

    public ModeleChronometre(){
        start = false;
        chrono = 0;
        chrono2 = 0;
        dureeMls = 0;
    }
    // Lancement du chrono
    public void start() {
        chrono = java.lang.System.currentTimeMillis() ;
        start = true ;
    }

    public boolean isStart(){ return start; }

    // Arrêt du chrono
    public void stop() {
        chrono2 = java.lang.System.currentTimeMillis() ;
        dureeMls = chrono2 - chrono ;
    }


    public long getTempsCentiemes(){
        chrono2 = java.lang.System.currentTimeMillis();
        dureeMls = chrono2 - chrono;
        return dureeMls / 10;
    }


    public String getTempsAffich(){
        String cent = String.format("%02d", this.getTempsCentiemes() % 100);
        String sec = String.format("%02d", (this.getTempsCentiemes() / 100) % 60);
        String min = String.format("%02d", (this.getTempsCentiemes() / 100) / 60);
        return min + ":" + sec + ":" + cent;
    }
}
