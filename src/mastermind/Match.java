package mastermind;

import java.util.Date;

public class Match {
    private ModeleJoueurM gagnant, perdant;
    private Date date;
    private int ptsGagnant, ptsPerdant;
    private boolean result;

    Match(){
        gagnant = new ModeleJoueurM("mdr");
        perdant = new ModeleJoueurM("nul");
        date = new Date();
        ptsGagnant = 0;
        ptsPerdant = 0;
        result = true;
    }

    ModeleJoueurM getGagnant(){ return gagnant; }

    ModeleJoueurM getPerdant(){ return perdant; }

    Date getDate(){ return date; }

    int getPtsGagnant(){ return ptsGagnant; }

    int getPtsPerdant(){ return ptsPerdant; }

    String getResult(){
        if (result){ return "Gagné !"; }
        else { return "Perdu..."; }
    }
}
