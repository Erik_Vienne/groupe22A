package mastermind;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.util.ArrayList;
import java.util.List;

public class VueGrille {

    private ArrayList<BorderPane> lstLigne;
    private int width, height;
    private VueApplication app;
    private VBox panel;

    public VueGrille(VueApplication app, int width, int height) {
        this.lstLigne = new ArrayList<>();
        this.app = app;
        this.width = width;
        this.height = height;
        this.setGrille();
    }

    public VBox getScene() {
        return this.panel;
    }

    private void setGrille() {
        this.panel = new VBox();
        this.panel.setId("grille");
        this.panel.setSpacing(10);
        this.panel.setPadding(new Insets(10,15,10,15));
        BorderPane.setMargin(this.panel,new Insets(20,0,20,40));
        this.panel.setMaxWidth(this.width/3);
        this.panel.setAlignment(Pos.BOTTOM_LEFT);
    }

    void insererLigne(List<String> lst, int bon, int mauv) {
        BorderPane ligne = new BorderPane();
        ligne.setId("ligne");
        ligne.setPadding(new Insets(9,0,9,20));

        HBox pions = new HBox();
        pions.setSpacing(25);

        for(String coul : lst) {
            pions.getChildren().add(new Circle(this.width/80,this.coulToPaint(coul)));
        }

        HBox res = new HBox();
        res.setAlignment(Pos.CENTER_RIGHT);
        res.setPadding(new Insets(0,20,0,0));
        res.setSpacing(15);

        for(int i = 0; i < bon; i++) {
            res.getChildren().add(new Circle(this.width/150, Color.rgb(255,255,255)));
        }

        for(int i = 0; i < mauv; i++) {
            res.getChildren().add(new Circle(this.width/150, Color.rgb(180,0,0)));
        }

        ligne.setLeft(pions);
        ligne.setRight(res);

        this.lstLigne.add(ligne);
        this.update();
    }

    private Color coulToPaint(String strg) {
        Color res = Color.rgb(0,0,0);
        switch(strg) {
            case("1"): res = Color.rgb(170,0,150); break;
            case("2"): res = Color.rgb(255,255,255); break;
            case("3"): res = Color.rgb(0,170,0); break;
            case("4"): res = Color.rgb(200,200,0); break;
            case("5"): res = Color.rgb(180,0,0); break;
            case("6"): res = Color.rgb(0,0,180); break;
        }
        return res;
    }

    private void update() {
        this.panel.getChildren().clear();
        if(this.lstLigne.size() > 7)
            lstLigne.remove(0);
        for(BorderPane pan : this.lstLigne) {
            this.panel.getChildren().add(pan);
        }
    }

    public void reset() {
        this.panel.getChildren().clear();
        this.lstLigne.clear();
    }

}
