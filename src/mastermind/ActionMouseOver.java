package mastermind;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class ActionMouseOver implements EventHandler<MouseEvent> {

    private VueApplication app;

    ActionMouseOver(VueApplication app) {
        this.app = app;
    }

    @Override
    public void handle(MouseEvent event) {
        this.app.hoverSound();
    }

}
