package mastermind;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

class ActionComboJeu implements EventHandler<ActionEvent> {

    private VueApplication app;
    private VueJeuSolo jeu;

    ActionComboJeu(VueApplication app, VueJeuSolo jeu) {
        this.app = app;
        this.jeu = jeu;
    }

    @Override
    public void handle(ActionEvent event) {
        this.app.clickSound();
        this.jeu.setEss(false);
        this.jeu.updatePions();
    }
}
