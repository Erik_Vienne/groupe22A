package mastermind;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

class ActionTemps implements EventHandler<ActionEvent> {

    private VueJeuSolo vueJeu;

    ActionTemps(VueJeuSolo vj){
        this.vueJeu = vj;
    }

    @Override
    public void handle(ActionEvent event) {
        this.vueJeu.updateInfo();
    }
}
