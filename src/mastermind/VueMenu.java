package mastermind;

import javafx.animation.FadeTransition;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

import java.io.File;
import java.util.ArrayList;

class VueMenu {

    private int width;
    private int height;
    private BorderPane panel;

    private VueApplication app;

    VueMenu(VueApplication app, int widht, int height) {
        this.app = app;
        this.height = height;
        this.width = widht;
        this.setMenu();
    }

    Parent getScene() {
        return this.panel;
    }

    private void setMenu() {

        // Création du titre
        FlowPane titre = new FlowPane();
        titre.setAlignment(Pos.TOP_CENTER);
        titre.setPadding(new Insets(this.height/5,0,0,0));

        Image titreImage = new Image( new File(Info.FILEPATH+"img/titre"+Info.APPCOLOR+".png").toURI().toString());

        ImageView titreAff = new ImageView();
        titreAff.setImage(titreImage);
        titreAff.setPreserveRatio(true);
        titreAff.setSmooth(true);

        FadeTransition ft = new FadeTransition(Duration.millis(2000), titreAff);
        ft.setFromValue(1.0);
        ft.setToValue(0.3);
        ft.setCycleCount(Timeline.INDEFINITE);
        ft.setAutoReverse(true);
        ft.play();

        titre.getChildren().add(titreAff);


        // Création des boutons
        VBox boutons = new VBox();
        boutons.setSpacing(13);
        boutons.setPadding(new Insets(0,0,this.height/20,0));
        boutons.setAlignment(Pos.BOTTOM_CENTER);

        ActionBoutonMenu actBut = new ActionBoutonMenu(this.app);

        ArrayList<Button> lstBut = new ArrayList<>();

        lstBut.add(new Button("Inviter un ami"));
        lstBut.add(new Button("Jouer en solo"));
        lstBut.add(new Button("Scores"));
        lstBut.add(new Button("Paramètres"));
        lstBut.add(new Button("Quitter"));

        for(Button but : lstBut) {
            but.setPrefSize( 300,40);
            but.setOnAction(actBut);
            but.setOnMouseEntered(this.app.getHover());
            boutons.getChildren().add(but);
        }


        // Création du conteneur final
        this.panel = new BorderPane();
        this.panel.setTop(titre);
        this.panel.setCenter(boutons);
    }

}



class ActionBoutonMenu implements EventHandler<ActionEvent> {

    private VueApplication app;

    ActionBoutonMenu(VueApplication app) {
        this.app = app;
    }

    @Override
    public void handle(ActionEvent event) {
        this.app.clickSound();
        Button but = (Button) event.getTarget();
        switch (but.getText()) {
            case("Quitter"): Platform.exit(); break;
            case("Paramètres"): this.app.setOptions(); break;
            case("Scores"): this.app.setScores(); break;
            case("Jouer en solo"): this.app.setJeuSolo(); break;
        }
    }
}
