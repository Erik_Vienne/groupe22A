package mastermind;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.io.File;
import java.util.Date;

class VueScores {

    private int width;
    private int height;
    private BorderPane panel;

    private VueApplication app;

    VueScores(VueApplication app, int widht, int height) {
        this.app = app;
        this.height = height;
        this.width = widht;
        this.setScores();
    }

    Parent getScene() {
        return this.panel;
    }

    private void setScores() {

        // Instanciation des handler
        ActionBoutonScores actScr = new ActionBoutonScores(this.app);


        // Création du titre
        FlowPane titre = new FlowPane();
        titre.setPadding(new Insets(this.height/8,0,0,this.width/45));

        Image titreImage = new Image(new File(Info.FILEPATH+"img/scores"+Info.APPCOLOR+".png").toURI().toString());
        ImageView titreAff = new ImageView();
        titreAff.setImage(titreImage);
        titreAff.setSmooth(true);

        titre.getChildren().add(titreAff);


        // Création du tableau
        VBox tableauDesScores = new VBox();
        TableView table = new TableView();

        TableColumn adv = new TableColumn("Adversaire");
        adv.setMinWidth(200);
        TableColumn date = new TableColumn("Date");
        date.setMinWidth(120);
        TableColumn score = new TableColumn("Votre score");
        score.setMinWidth(150);
        TableColumn scoreAdv = new TableColumn("Score de l'adversaire");
        scoreAdv.setMinWidth(200);
        TableColumn result = new TableColumn("Résultat");
        result.setMinWidth(120);


        table.getColumns().addAll(adv, date, score, scoreAdv, result);

        ObservableList<Match> scores = FXCollections.observableArrayList(new Match(), new Match(), new Match(), new Match());
        adv.setCellValueFactory(new PropertyValueFactory<Match, ModeleJoueurM>("Adversaire"));
        date.setCellValueFactory(new PropertyValueFactory<Match, Date>("Date"));
        score.setCellValueFactory(new PropertyValueFactory<Match, Integer>("Votre score"));
        scoreAdv.setCellValueFactory(new PropertyValueFactory<Match, Integer>("Score de l'adversaire"));
        result.setCellValueFactory(new PropertyValueFactory<Match, String>("Résultat"));


        table.setItems(scores);
        table.setMaxWidth(adv.getWidth() + date.getWidth() + score.getWidth() + scoreAdv.getWidth() + result.getWidth() + 4);

        tableauDesScores.getChildren().add(table);
        tableauDesScores.setAlignment(Pos.CENTER);


        // Création des boutons du bas
        HBox bas = new HBox();
        bas.setPadding(new Insets(0,0,this.height/30,this.width/45));

        Button ret = new Button("Retour");
        ret.setPrefSize(250,40);
        ret.setOnAction(actScr);
        ret.setOnMouseEntered(this.app.getHover());

        bas.getChildren().add(ret);


        // Création du conteneur final
        this.panel = new BorderPane();
        this.panel.setTop(titre);
        this.panel.setCenter(tableauDesScores);
        this.panel.setBottom(bas);
    }

}



class ActionBoutonScores implements EventHandler<ActionEvent> {

    private VueApplication app;

    ActionBoutonScores(VueApplication app) {
        this.app = app;
    }

    @Override
    public void handle(ActionEvent event) {
        this.app.clickSound();
        Button but = (Button) event.getTarget();
        switch (but.getText()) {
            case("Retour"): this.app.setMenu(); break;
        }
    }
}
