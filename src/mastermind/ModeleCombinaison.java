package mastermind;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModeleCombinaison {

    // Crée une combinaison avec quatre variables de type String, correspondant à des chiffres entre 1 et 6.
    private String un, deux, trois, quatre;

    // Constructeur avec les quatre variables en clair
    ModeleCombinaison(String un, String deux, String trois, String quatre){
        this.un = un;
        this.deux = deux;
        this.trois = trois;
        this.quatre = quatre;
    }
    // Constructeur avec une liste de quatre String
    ModeleCombinaison(List<String> liste){
        this.un = liste.get(0);
        this.deux = liste.get(1);
        this.trois = liste.get(2);
        this.quatre = liste.get(3);
    }

    List<String> getListe(){
        List<String> lst = new ArrayList<>();
        lst.add(un);
        lst.add(deux);
        lst.add(trois);
        lst.add(quatre);
        return lst;
    }

    int getNbBon(ModeleCombinaison comb) {
        int res = 0;
        for(int i = 0; i < 4; i++) {
            if(comb.getListe().get(i).equals(this.getListe().get(i)))
                res++;
        }
        return res;
    }

    // Fonction qui retourne le nombre de pions mal placés dans la combinaison comb par rapport à celle ci
    int getNbMau(ModeleCombinaison comb) {
        int res = 0;

        Map<String, Integer> combCont = new HashMap<>();
        combCont.clear();
        for(String coul : this.getListe()) {
            combCont.put(coul, combCont.getOrDefault(coul,0) + 1);
        }

        Map<String, Integer> bonCont = new HashMap<>();
        bonCont.clear();
        for(int i = 0; i < 4; i++) {
            if(comb.getListe().get(i).equals(this.getListe().get(i)))
                bonCont.put(comb.getListe().get(i), bonCont.getOrDefault(comb.getListe().get(i),0) + 1);
        }

        for(int i = 0; i < 4; i++) {
            String coul = comb.getListe().get(i);
            if(this.getListe().contains(coul) && !this.getListe().get(i).equals(coul)) {
                if(bonCont.getOrDefault(coul,0) < combCont.getOrDefault(coul,0)) {
                    res++;
                    bonCont.put(coul,bonCont.getOrDefault(coul,0));
                }
            }
        }

        return res;
    }

    // Permet de vérifier si une combinaison est égale à une autre
    @Override
    public boolean equals(Object o){
        if(o == null) return  false;
        else {
            ModeleCombinaison c = (ModeleCombinaison) o;
            return this.un.equals(c.un) && this.deux.equals(c.deux) && this.trois.equals(c.trois) && this.quatre.equals(c.quatre);
        }
    }

    // Permet d'afficher la combinaison pour les tests
    public String toString(){
        return un + " - " + deux + " - " + trois + " - " + quatre;
    }
}
