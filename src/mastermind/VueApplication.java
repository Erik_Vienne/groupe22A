package mastermind;

import javafx.animation.*;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;


public class VueApplication extends Application {

    private Stage stage;
    private Scene main;
    private Parent menu;
    private Parent opt;
    private Parent scores;
    private Parent jeuSolo;
    private Parent current;
    private MusicPlayer music;
    private ActionMouseOver hover;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void init() throws Exception{
        super.init();

        // Chargement des sons
        this.music = new MusicPlayer();
        this.hover = new ActionMouseOver(this);

        // Chargement des paramètres
        Info.load();

        // Création des couleurs
        Info.NORMCOLOR.put("Vert",Color.rgb(0,150,0,1.0));
        Info.NORMCOLOR.put("Bleu",Color.rgb(0,30,180,1.0));
        Info.NORMCOLOR.put("Orange", Color.rgb(255,100,0));
        Info.DARKCOLOR.put("Vert",Color.rgb(0,80,0,1.0));
        Info.DARKCOLOR.put("Bleu",Color.rgb(0,30,80,1.0));
        Info.DARKCOLOR.put("Orange", Color.rgb(150,40,0));

    }

    @Override
    public void start(Stage fco){
        this.stage = new Stage();
        this.stage.setResizable(false);
        this.stage.setFullScreen(Info.FULLSCREEN);
        this.stage.setFullScreenExitHint("");

        // Chargement des scènes
        this.menu = new VueMenu(this, Info.WIDTH, Info.HEIHT).getScene();
        this.opt = new VueOptions(this, Info.WIDTH, Info.HEIHT).getScene();
        this.scores = new VueScores(this, Info.WIDTH, Info.HEIHT).getScene();
        this.current = this.menu;

        this.main = new Scene(this.menu, Info.WIDTH,Info.HEIHT, Color.rgb(0,0,0));
        this.main.getStylesheets().add(getClass().getResource("Main"+Info.APPCOLOR+".css").toExternalForm());


        this.stage.setScene(this.main);
        this.stage.show();
        this.music.playMenu();

    }


    void setMenu() {
        this.transition(this.current, this.menu);
        this.current = this.menu;
}

    void setOptions()  {
        this.transition(this.current, this.opt);
        this.current = this.opt;
    }

    void setScores() {
        this.transition(this.current, this.scores);
        this.current = this.scores;
    }

    void setJeuSolo() {
        this.jeuSolo = new VueJeuSolo(this,Info.WIDTH,Info.HEIHT).getScene();
        this.transition(this.current, this.jeuSolo);
        this.current = this.jeuSolo;
    }


    void reload() {
        this.menu = new VueMenu(this, Info.WIDTH, Info.HEIHT).getScene();
        this.opt = new VueOptions(this, Info.WIDTH, Info.HEIHT).getScene();
        this.scores = new VueScores(this,Info.WIDTH,Info.HEIHT).getScene();
        this.current = this.opt;
        this.main = new Scene(this.opt, Info.WIDTH, Info.HEIHT, Color.rgb(0,0,0));

        this.main.getStylesheets().clear();
        this.main.getStylesheets().add(getClass().getResource("Main"+Info.APPCOLOR+".css").toExternalForm());

        this.stage.setScene(this.main);
        this.stage.setFullScreen(Info.FULLSCREEN);
        this.stage.show();
    }

    private void transition(Parent scene1, Parent scene2) {
        SequentialTransition seq = new SequentialTransition();

        KeyValue kv1Scene1 = new KeyValue(scene1.opacityProperty(), 1.0);
        KeyValue kv2Scene1 = new KeyValue(scene1.opacityProperty(), 0.0);
        KeyValue kvChange = new KeyValue(this.main.rootProperty(), scene2);
        KeyValue kv1Scene2 = new KeyValue(scene2.opacityProperty(), 0.0);
        KeyValue kv2Scene2 = new KeyValue(scene2.opacityProperty(), 1.0);
        KeyFrame kf1Scene1 = new KeyFrame(Duration.millis(0),kv1Scene1);
        KeyFrame kf2Scene1 = new KeyFrame(Duration.millis(400),kv2Scene1);
        KeyFrame kfChange = new KeyFrame(Duration.millis(401), kvChange);
        KeyFrame kf1Scene2 = new KeyFrame(Duration.millis(402),kv1Scene2);
        KeyFrame kf2Scene2 = new KeyFrame(Duration.millis(802),kv2Scene2);
        Timeline trans1 = new Timeline();
        trans1.getKeyFrames().add(kf1Scene1);
        trans1.getKeyFrames().add(kf2Scene1);
        trans1.getKeyFrames().add(kfChange);
        trans1.getKeyFrames().add(kf1Scene2);
        trans1.getKeyFrames().add(kf2Scene2);

        seq.getChildren().add(trans1);

        seq.play();
    }

    void clickSound() { this.music.playClick(); }

    void hoverSound() { this.music.playHover(); }

    void updateMusVol() { this.music.updateMus(); }

    void updateSndVol() { this.music.updateSnd(); }

    ActionMouseOver getHover() { return this.hover; }
}
