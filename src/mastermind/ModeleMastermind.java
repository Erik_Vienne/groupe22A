package mastermind;

import java.util.ArrayList;
import java.util.List;

public class ModeleMastermind{

    private ModeleChronometre chrono; // chronomètre
    private ModeleJoueurM joueur; // Les deux joueurs + le gagnant actuel
    private ModeleCombinaison combinaison; // La combinaison à trouver
    private int nbPointsTotal, nbEssais, nbPoints;
    private boolean partieFinie; // Montre si la partie est finie
    private int numManche;

    ModeleMastermind(ModeleJoueurM joueurN1){
        joueur = joueurN1; // Initialisation du joueur
        partieFinie = false;
        this.nouvCombinaison();
        chrono = new ModeleChronometre(); // Création du chronomètre
        numManche = 1;
        this.nbEssais = 0;
        this.nbPoints = 1000;
        this.nbPointsTotal = 0;
    }

    int getNumManche(){return numManche;}

    ModeleJoueurM getJoueur(){return joueur;}

    ModeleChronometre getChrono(){return chrono;}

    String getNbEssais() {
        return String.valueOf(this.nbEssais);
    }

    String getNbPoints() {
        return String.valueOf(this.getPoints());
    }

    String getNbPointsTotal() {
        return String.valueOf(this.nbPointsTotal);
    }

    // Get le temps écoulé en secondes
    private long getTime() {
        return chrono.getTempsCentiemes() / 10;
    }

    ModeleCombinaison getCombi() {
        return combinaison;
    }

    boolean isPartieFinie() {
        return partieFinie;
    }

    public List<Integer> isCombiValide(ModeleCombinaison combiTest){
        List<Integer> lst = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            if (combinaison.getListe().get(i) == combiTest.getListe().get(i)) {
                lst.add(2);
            }
            else if (combiTest.getListe().contains(combinaison.getListe().get(i))) {
                lst.add(1);
            }
            else {
                lst.add(0);
            }
        }
        return lst;
    }

    private void finPartie() {
        // Upload sur la BD les résultats du match si le jeu est en multi
        this.partieFinie = true;
    }

    private void nouvCombinaison() {
        List<String> listeCombi = new ArrayList<>();
        for(int i = 0; i < 4; i++){
            int nb = (int)(Math.random() * 6) + 1;
            listeCombi.add(Integer.toString(nb));
        }
        this.combinaison = new ModeleCombinaison(listeCombi); // On crée la combinaison avec la liste listeCombi
    }

    void mancheFinie(){
        if(numManche < 3) {
            numManche ++;
            this.nouvCombinaison();
            this.nbPointsTotal+=this.nbPoints;
            this.nbPoints = 0;
            this.nbEssais = 0;
            this.chrono = new ModeleChronometre();
        }
        else {
            this.finPartie();
        }
    }

    private long getPoints() {
        this.nbPoints = (1000 - (this.nbEssais * 50 + (int) this.getTime()));
        return this.nbPoints;
    }

    boolean essaiCombi() {
        this.nbEssais++;
        return (this.joueur.getCombiEnCours().equals(this.combinaison));
    }

}
