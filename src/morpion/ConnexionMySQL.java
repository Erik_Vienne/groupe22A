package morpion;

import java.sql.*;
import java.util.Properties;

/**
 * Une classe interface entre l'application et la base de données
 */
public class ConnexionMySQL {
    private Connection mysql;
    private boolean connecte=false;
    public ConnexionMySQL() throws ClassNotFoundException{
        Class.forName("com.mysql.cj.jdbc.Driver");
    }

    public void connecter(String nomServeur, String nomBase, String nomLogin, String motDePasse) throws SQLException {
        Properties info = new Properties();
        info.put("user", nomLogin);
        info.put("password", motDePasse);

        info.put("serverTimezone", "GMT");
        this.mysql = DriverManager.getConnection("jdbc:mysql://"+nomServeur+":3306/"+nomBase, info);
        this.connecte = true;
    }

    public void close() throws SQLException {
        this.mysql.close();
        this.connecte=false;
    }

    public boolean isConnecte(){ return this.connecte;}

    public Blob createBlob() throws SQLException{
        return this.mysql.createBlob();
    }

    public Statement createStatement() throws SQLException {
        return this.mysql.createStatement();
    }

    public PreparedStatement prepareStatement(String requete) throws SQLException{
        return this.mysql.prepareStatement(requete);
    }

}
