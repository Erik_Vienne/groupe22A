package morpion;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

/**
 * Une bibliothèque contenant diverses fonctions permettant au jeu d'intéragir avec la base de données.
 */
public class BibliMySQLMorpion {
    /**
     * Renvoie une ConnexionMySQL connectée à la base de données.
     * @return une ConnexionMySQL connectée à la base de données, null en cas d'erreur.
     */
    public static ConnexionMySQL getConnexion(){
        try {
            ConnexionMySQL c = new ConnexionMySQL();
            c.connecter("servinfo-db", "dbsleroy", "sleroy", "good_password");
            return c;
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e);
            return null;
        }
    }

    public static int idPaMax(ConnexionMySQL c) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        ResultSet rs = c.createStatement().executeQuery("select max(idPa) from PARTIE");
        return rs.getInt(1);
    }

    public static void initPartie(ConnexionMySQL c, int idJoueurDebut) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        PreparedStatement ps = c.prepareStatement("update PARTIE set donneesPa=? where idPa=?");
        ps.setInt(2, BibliMySQLMorpion.idPaMax(c));
        JSONObject jo = new JSONObject();
        JSONArray l = new JSONArray();
        for (int i=0; i<3; i++){
            JSONArray li = new JSONArray();
            li.addAll(Arrays.asList("","",""));
            l.add(li);
        }
        jo.put("grille", l);
        jo.put("joueurCourant", idJoueurDebut);
        ps.setString(2, l.toString());
        ps.executeUpdate();
    }

    public static void setGrille(ConnexionMySQL c, String[][] grille, int idJoueurCourant, int idPa) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        PreparedStatement ps = c.prepareStatement("update PARTIE set donneesPa=? where idPa=?");
        ps.setInt(2, idPa);
        JSONObject jo = new JSONObject();
        JSONArray l = new JSONArray();
        for (String[] ligne:grille){
            JSONArray li = new JSONArray();
            li.addAll(Arrays.asList(ligne));
            l.add(li);
        }
        jo.put("grille", l);
        jo.put("joueurCourant", idJoueurCourant);
        ps.setString(2, l.toString());
        ps.executeUpdate();
    }

    public static String[][] getGrille(ConnexionMySQL c, int idPa) throws SQLException, ParseException {
        if (!c.isConnecte())
            throw new SQLException();
        ResultSet rs = c.createStatement().executeQuery("select donneesPa from PARTIE where idPa="+idPa);
        String donnees = rs.getString("donneesPa");
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject)parser.parse(donnees);
        JSONArray g = (JSONArray)json.get("grille");

        String[][] grille = new String[3][3];
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                grille[i][j] = (String)((JSONArray)g.get(i)).get(j);
            }
        }

        return grille;
    }
}
