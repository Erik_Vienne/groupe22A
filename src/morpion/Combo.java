package morpion;
public class Combo {
    public Case[] cases;
    private TicTacToeApp app;

    public Combo(TicTacToeApp app, Case...cases){
        this.app = app;
        this.cases = cases;
    }

    public boolean estComplet(){
        if(cases[0].getValeur().isEmpty())
            return false;
        return cases[0].getValeur().equals(cases[1].getValeur()) && cases[0].getValeur().equals(cases[2].getValeur());
    }
}
