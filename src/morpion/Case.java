package morpion;

import javafx.geometry.Pos;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.sql.SQLException;
import java.util.Arrays;


public class Case extends StackPane {
    private Text text = new Text();
    private TicTacToeApp app;
    private boolean utilisee = false;
    private int x, y;

    public Case(TicTacToeApp app, int x, int y){
        this.x = x;
        this.y = y;
        this.app = app;
        Rectangle border = new Rectangle(200,200);
        border.setFill(null);
        border.setStroke(Color.BLACK);

        text.setFont(Font.font(80));
        setAlignment(Pos.CENTER);
        getChildren().addAll(border, text);
        setOnMouseClicked(event ->{
            if(!app.jouable || utilisee){
                return;
            }
//            if(event.getButton() == MouseButton.PRIMARY){
            if (app.morpion.idUt==1){
//                if(!app.tourX)
//                    return;

                dessinX();
                app.tourX = false;
                app.grille[x][y] = getValeur();
                utilisee = true;
                try {
                    BibliMySQLMorpion.setGrille(app.morpion.mySQL, app.grille, app.morpion.idUt, app.morpion.idPa);
                } catch (SQLException e){
                    e.printStackTrace();
                }
                app.verifEtat();
                System.out.println(app.grille[x][y]);
            }
//            else if(event.getButton() == MouseButton.SECONDARY){
            else{
//                if(app.tourX)
//                    return;

                dessinO();
                app.tourX = true;
                app.grille[x][y] = getValeur();
                utilisee = true;
                try {
                    BibliMySQLMorpion.setGrille(app.morpion.mySQL, app.grille, app.morpion.idUt, app.morpion.idPa);
                } catch (SQLException e){
                    e.printStackTrace();
                }
                app.verifEtat();
                System.out.println(app.grille[x][y]);
            }
        });
    }

    public String getValeur(){
        return text.getText();
    }

    public void dessinX(){
        text.setText("X");
    }
    public void dessinO(){
        text.setText("O");
    }
    public double getCentreX(){
        return getTranslateX() + 100;
    }
    public double getCentreY(){
        return getTranslateY() + 100;
    }

    public int getX() {return x;}

    public int getY() {return y;}

    @Override
    public String toString() {
        return this.text.getText();
    }
}
