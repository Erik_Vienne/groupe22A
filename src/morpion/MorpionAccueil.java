package morpion;

import client.BibliMySQLClient;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.List;

public class MorpionAccueil extends Application {
    private Stage primaryStage;

    public ConnexionMySQL mySQL;

    public int idUt;
    public int idPa;

    public BorderPane createContent() {
        BorderPane bp = new BorderPane();
        Label titre = new Label("Morpion");
        titre.setFont(Font.font(100));
        titre.setAlignment(Pos.CENTER);


        Button jouer = new Button("Jouer");
        jouer.setOnAction(actionEvent -> {
            try {
                BibliMySQLMorpion.setGrille(mySQL, new String[][]{new String[]{" ", " ", " "}, new String[]{" ", " ", " "},new String[]{" ", " ", " "}}, this.idUt, this.idPa);
            } catch (Exception e){e.printStackTrace();}
            TicTacToeApp jeu = new TicTacToeApp(primaryStage, this);
            this.primaryStage.setScene(new Scene(jeu.createContent()));
        });
        jouer.setPadding(new Insets(10));

        Button score = new Button("Score");
        score.setPadding(new Insets(10));

        Button quitter = new Button("Quitter");
        quitter.setOnAction(actionEvent -> {
            primaryStage.hide();
        });
        quitter.setPadding(new Insets(10));

        //Tailles
        jouer.setPrefSize(100.0,50.0);
        score.setPrefSize(100.0,50.0);
        quitter.setPrefSize(100.0,50.0);

        //taille police
        Font fontBouton=new Font(20);
        jouer.setFont(fontBouton);
        score.setFont(fontBouton);
        quitter.setFont(fontBouton);

        VBox menu= new VBox();
        menu.getChildren().addAll(jouer, score, quitter);
        menu.setAlignment(Pos.CENTER);
        menu.setSpacing(30);

        bp.setTop(titre);
        bp.setCenter(menu);
        return bp;
    }

    @Override
    public void stop() throws Exception {
        this.mySQL.close();
    }

    @Override
    public void start(Stage primaryStage) {
        this.mySQL = BibliMySQLMorpion.getConnexion();
        this.primaryStage = primaryStage;
        primaryStage.setScene(new Scene(createContent(), 600,600));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public void demarrer(int idPa, int idUt){
        this.idPa=idPa;
        this.idUt=idUt;
        this.start(new Stage());
    }

    public static void main(String[] args) {
        launch(args);
    }
}
