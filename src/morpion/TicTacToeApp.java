package morpion;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.json.simple.parser.ParseException;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class TicTacToeApp {

    public boolean jouable = true;
    public boolean tourX = true;
    private List<Combo> combos = new ArrayList<>();
    private Case[][] tableau = new Case[3][3];
    public String[][] grille = new String[3][3];
    private Pane root = new Pane();
    private Stage primaryStage;
    public MorpionAccueil morpion;
    private Timeline refresh;

    public TicTacToeApp(Stage primaryStage, MorpionAccueil morpion) {
        this.primaryStage = primaryStage;
        this.morpion = morpion;
        refresh = new Timeline(new KeyFrame(
                Duration.millis(500),
                e ->{
                    try{
                        grille = BibliMySQLMorpion.getGrille(morpion.mySQL, morpion.idPa);
                    } catch (ParseException | SQLException ex){ex.printStackTrace();}
                    majGrille();
                    verifEtat();
                }));

        refresh.setCycleCount(Animation.INDEFINITE);
        refresh.play();

    }

    public Parent createContent() {
        root.setPrefSize(600, 600);

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Case caseJeu = new Case(this, j, i);
                caseJeu.setTranslateX(j * 200);
                caseJeu.setTranslateY(i * 200);

                root.getChildren().add(caseJeu);

                grille[j][i] = caseJeu.getValeur();
                tableau[j][i] = caseJeu;
            }
        }
        //horizontal
        for (int y = 0; y < 3; y++) {
            combos.add(new Combo(this, tableau[0][y], tableau[1][y], tableau[2][y]));
        }

        //vertical
        for (int x = 0; x < 3; x++) {
            combos.add(new Combo(this, tableau[x][0], tableau[x][1], tableau[x][2]));
        }

        //diagonales
        combos.add(new Combo(this, tableau[0][0], tableau[1][1], tableau[2][2]));
        combos.add(new Combo(this, tableau[2][0], tableau[1][1], tableau[0][2]));

        return root;
    }

    public void verifEtat() {
        for (Combo combo : combos) {
            if (combo.estComplet()) {
                jouable = false;
                refresh.stop();
                playWinAnimation(combo);
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Fin de partie");
                alert.setHeaderText("Que voulez-vous faire ?");

                ButtonType buttonTypeRejouer = new ButtonType("Rejouer");
                ButtonType buttonTypeAccueil = new ButtonType("Accueil");

                alert.getButtonTypes().setAll(buttonTypeRejouer, buttonTypeAccueil);

                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == buttonTypeRejouer) {
                    // ... Rejouer
                    TicTacToeApp jeu = new TicTacToeApp(this.primaryStage, morpion);
                    this.primaryStage.setScene(new Scene(jeu.createContent()));
                } else{
                    // ... Accueil
                    this.primaryStage.setScene(new Scene(new MorpionAccueil().createContent(), 600, 600));
                }
                break;
            }
        }
    }

    private void playWinAnimation(Combo combo) {
        Line ligne = new Line();
        ligne.setStartX(combo.cases[0].getCentreX());
        ligne.setStartY(combo.cases[0].getCentreY());

        ligne.setEndX(combo.cases[0].getCentreX());
        ligne.setEndY(combo.cases[0].getCentreY());

        ligne.setStroke(Color.RED);
        ligne.setStrokeWidth(5);


        root.getChildren().add(ligne);

        Timeline timeline = new Timeline();
        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1),
                new KeyValue(ligne.endXProperty(), combo.cases[2].getCentreX()),
                new KeyValue(ligne.endYProperty(), combo.cases[2].getCentreY())));
        timeline.play();

        refresh.stop();


    }
    private void majGrille(){
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if(grille[j][i].equals("X"))
                    tableau[j][i].dessinX();
                if(grille[j][i].equals("O"))
                    tableau[j][i].dessinO();
            }
        }
    }
}

