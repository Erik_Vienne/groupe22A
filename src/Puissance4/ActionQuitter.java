package Puissance4;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

/**
 * Controller qui permet de quitter le jeu
 */
public class ActionQuitter implements EventHandler<ActionEvent> {

    private Puissance4Vue p4v;

    /**
     * Contructeur du controller qui permet de quitter
     */
    public ActionQuitter(){
        this.p4v = p4v;
    }


    @Override
    public void handle(ActionEvent event){
        Platform.exit();
    }
}
