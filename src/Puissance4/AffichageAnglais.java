package Puissance4;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Controller qui permet d'afficher le jeu en anglais
 */
public class AffichageAnglais implements EventHandler<ActionEvent> {

    private Puissance4Vue p4v;

    /**
     * Constructeur du controller qui permet d'affciher le jeu en anglais
     * @param p4v
     */
    public AffichageAnglais(Puissance4Vue p4v){
        this.p4v=p4v;
    }

    @Override
    public void handle(ActionEvent event){
        p4v.setStage(p4v.jeuEn(),"Connect 4");
        this.p4v.getChrono().resetTime();
        this.p4v.getChrono().start();
    }
}