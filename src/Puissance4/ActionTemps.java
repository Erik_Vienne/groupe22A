package Puissance4;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;

/**
 * Contrôleur du chronomètre
 */
public class ActionTemps implements EventHandler<ActionEvent> {

    /**
     * temps restant depuis le début de la mesure
     */
    private long tempsRestant;
    /**
     * Vue du chronomètre
     */
    private Chronometre chrono;
    private Puissance4Vue p4v;

    /**
     * Constructeur du controller du chronomètre
     * @param chrono
     * @param p4v
     */
    ActionTemps (Chronometre chrono, Puissance4Vue p4v){
        this.chrono = chrono;
        this.p4v=p4v;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
	    tempsRestant-=100;
	    this.chrono.setTime(tempsRestant);
	    if(tempsRestant==0) {
            this.chrono.stop();
            Alert tempsEcoule=new Alert(Alert.AlertType.INFORMATION);
            tempsEcoule.setTitle("ATTENTION");
            tempsEcoule.setHeaderText("TEMPS ECOULE");
            tempsEcoule.setContentText("Absence de "+this.p4v.getModele().getCourant()+". Fin de la partie.");
            tempsEcoule.show();
            this.p4v.setStage(this.p4v.menuFr(),"Menu");
        }
    }

    /**
     * renvoie le temps restant
     * @return le temps restant
     */
    public int getTemps(){
        return (int) this.tempsRestant;
    }

    /**
     * Remet la durée à 60
     */
    public void reset(){
	    this.chrono.setTime(60000);
	    this.tempsRestant=60000;
    }
}
