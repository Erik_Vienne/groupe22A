package Puissance4;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Controller qui permet d'accéder au Puissance 4 en français
 */
public class ActionAffichage implements EventHandler<ActionEvent> {

    private Puissance4Vue p4v;

    /**
     * Constructeur du controller pour afficher le jeu en français
     * @param p4v
     */
    public ActionAffichage(Puissance4Vue p4v){
        this.p4v=p4v;
    }

    @Override
    public void handle(ActionEvent event){
        p4v.setStage(p4v.jeuFr(),"Puissance 4");
        this.p4v.getChrono().resetTime();
        this.p4v.getChrono().start();
    }
}
