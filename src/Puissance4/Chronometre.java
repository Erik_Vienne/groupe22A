package Puissance4;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.text.Font;
import javafx.util.Duration;

/**
 * Permet de gérer un label associé à une Timeline pour afficher un temps écoulé
 */
public class Chronometre extends Label{
    /**
     * timeline qui va gérer le temps
     */
    private Timeline timeline;
    /**
     * la fenêtre de temps
     */
    private KeyFrame keyFrame;
    /**
     * le contrôleur associé au chronomètre
     */
    private ActionTemps actionTemps;

    private Puissance4Vue p4v;

    /**
     * Constructeur permettant de créer le chronomètre
     * avec un label initialisé à "30"
     * Ce constructeur créer la Timeline, la KeyFrame et le contrôleur
     */
    Chronometre(Puissance4Vue p4v){
	super("60");
	this.p4v=p4v;
	this.setFont(Font.font("Ubuntu",30.0));
	this.actionTemps=new ActionTemps(this,this.p4v);
	this.keyFrame = new KeyFrame(Duration.millis(100), this.actionTemps);
	this.timeline = new Timeline(keyFrame);
	this.timeline.setCycleCount(Animation.INDEFINITE);
	this.setTime(30);
    }

    /**
     * Permet au controleur de mettre à jour le label
     * la durée est affichée sous la forme h:m:s
     * @param tempsMillisec la durée depuis à afficher
     */
    public void setTime(long tempsMillisec){
        long tpsSec=tempsMillisec/1000;
        long sec=tpsSec%60;
        if(sec<10){
            this.setText("0"+sec);
        }
        else {
            this.setText("" + sec);
        }
    }

    /**
     *
     * @return le contrôleur du chronomètre
     */
    public ActionTemps getActionTemps(){
        return this.actionTemps;
    }

    /**
     * Permet de démarrer le chronomètre
     */
    public void start(){
        this.timeline.play();
    }

    /**
     * Permet d'arrêter le chronomètre
     */
    public void stop(){
       this.timeline.stop();
    }

    /**
     * Permet de remettre le chronomètre à 0
     */
    public void resetTime(){
       this.actionTemps.reset();
    }
}

