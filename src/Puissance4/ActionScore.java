package Puissance4;

import Puissance4.Puissance4Vue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Controller qui permet d'accéder à la page des scores
 */
public class ActionScore implements EventHandler<ActionEvent> {

    private Puissance4Vue p4v;
    private Score score;

    /**
     * Constructeur du controller qui permet d'accéder à la page des scores
     * @param p4v
     * @param score
     */
    public ActionScore(Puissance4Vue p4v, Score score){
        this.p4v=p4v;
        this.score=score;
    }

    @Override
    public void handle(ActionEvent event){
        p4v.setStage(p4v.score(),"Score");
    }
}