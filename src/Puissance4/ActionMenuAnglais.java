package Puissance4;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Controller qui permet d'affciher le menu en anglais
 */
public class ActionMenuAnglais implements EventHandler<ActionEvent> {

    private Puissance4Vue p4v;

    /**
     * Contructeur du controller qui affiche le menu en anglais
     * @param p4v
     */
    public ActionMenuAnglais(Puissance4Vue p4v){
        this.p4v=p4v;
    }

    @Override
    public void handle(ActionEvent event){
        p4v.setStage(p4v.menuEn(),"Connect 4");
    }
}