package Puissance4;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.File;

/**
 * la vue du puissance 4
 */
public class Puissance4Vue extends Application{

    /**
     * les joueurs
     */
    private Joueur j1, j2;
    /**
     * chronomètre
     */
    private Chronometre chrono;
    /**
     * ensemble des scènes
     */
    private Stage stage;
    /**
     * le modèle du puissance 4
     */
    private Puissance4 puissance4;
    /**
     * le plateau de jeu
     */
    private GridPane plateau;
    /**
     * le joueur courant
     */
    private String courant;
    /**
     * le score du joueur
     */
    private Score score;

//__________________________________Elements modulables______________________________

    /**
     * créé la HBox contenant les joueurs
     * @return une HBox
     */
    private HBox lesJoueurs(){
        HBox res=new HBox(5);
        Label joueurs=new Label(j1.getPseudo()+" VS "+j2.getPseudo()+"           |    Au tour de "+this.courant);
        Font fontJoueurs=new Font("Ubuntu",25);
        joueurs.setFont(fontJoueurs);

        res.getChildren().add(joueurs);
        joueurs.setPadding(new Insets(10));

        res.setStyle("-fx-background-color: white");

        return res;
    }

    /**
     * créé la VBox contenant la messagerie en français
     * @return une VBox
     */
    private VBox laMessagerieFr(){
        VBox res=new VBox(5);
        TextField message=new TextField();

        res.getChildren().add(message);
        res.setPadding(new Insets(805,5,5,5));

        res.setStyle("-fx-background-color: white");
        res.setAlignment(Pos.BOTTOM_CENTER);
        message.setPromptText("Entrer un message");

        return res;
    }

    /**
     * créé la VBox contenant la messagerie en anglais
     * @return une VBox
     */
    private VBox laMessagerieEn(){
        VBox res=new VBox(5);
        TextField message=new TextField();

        res.getChildren().add(message);
        res.setPadding(new Insets(805,5,5,5));

        res.setStyle("-fx-background-color: white");
        res.setAlignment(Pos.BOTTOM_CENTER);
        message.setPromptText("Enter a message");

        return res;
    }

    /**
     * créé la VBox contenant le chronomètre
     * @return une VBox
     */
    private VBox leChrono(){
        VBox res = new VBox(5);
        res.setAlignment(Pos.CENTER_LEFT);
        res.setPadding(new Insets(15,10,10,23));

        this.chrono=new Chronometre(this);
        res.getChildren().add(this.chrono);
        return res;
    }

    /**
     * getter du chronomètre
     * @return un chronomètre
     */
    public Chronometre getChrono() {
        return this.chrono;
    }

    /**
     * créé un GridPane qui représente le Puissance 4
     * @return un GridPane
     */
    public GridPane lePlateau(){
        this.plateau.setAlignment(Pos.CENTER);
        this.plateau.setStyle("-fx-background-color: blue");
        this.plateau.setPadding(new Insets(100,0,50,0));

        Button a = new Button("1");
        Button b = new Button("2");
        Button c = new Button("3");
        Button d = new Button("4");
        Button e = new Button("5");
        Button f = new Button("6");
        Button g = new Button("7");

        a.setPrefWidth(100.0);
        b.setPrefWidth(100.0);
        c.setPrefWidth(100.0);
        d.setPrefWidth(100.0);
        e.setPrefWidth(100.0);
        f.setPrefWidth(100.0);
        g.setPrefWidth(100.0);

        this.plateau.add(a,0,0);
        this.plateau.add(b,1,0);
        this.plateau.add(c,2,0);
        this.plateau.add(d,3,0);
        this.plateau.add(e,4,0);
        this.plateau.add(f,5,0);
        this.plateau.add(g,6,0);

        a.setOnAction(new ActionPoserPion(this,this.puissance4));
        b.setOnAction(new ActionPoserPion(this,this.puissance4));
        c.setOnAction(new ActionPoserPion(this,this.puissance4));
        d.setOnAction(new ActionPoserPion(this,this.puissance4));
        e.setOnAction(new ActionPoserPion(this,this.puissance4));
        f.setOnAction(new ActionPoserPion(this,this.puissance4));
        g.setOnAction(new ActionPoserPion(this,this.puissance4));

        for(int i=1 ; i<=6 ; i++ ){
            for(int j=0 ; j<7 ; j++){
                Circle blanc = new Circle(50);
                blanc.setFill(Color.WHITE);
                this.plateau.add(blanc,j,i);
                if(this.puissance4.getPions().size()!=0){
                    for(Pion pion : this.puissance4.getPions()){
                        Circle couleur=new Circle(50);
                        couleur.setFill(pion.getCouleur());
                        this.plateau.add(couleur,pion.getPosX(),pion.getPosY());
                    }
                }
            }
        }

        this.plateau.setHgap(10);
        this.plateau.setVgap(10);

        return this.plateau;
    }

//___________________________________Scènes__________________________________________

    //_________________Menus___________________

    /**
     * créé la scène du menu en français
     * @return une Scene
     */
    public Scene menuFr(){
        //Création des parties et éléments de l'interface
        Label titre=new Label("PUISSANCE 4");
        Button inviter=new Button("Inviter un ami");
        Button score=new Button("Scores");
        Button quitter=new Button("Quitter");
        BorderPane menu=new BorderPane();
        VBox scene=new VBox(5);
        ComboBox langue=new ComboBox();

        //Ajout des éléments dans les parties
        scene.getChildren().add(titre);
        scene.getChildren().add(inviter);
        scene.getChildren().add(score);
        scene.getChildren().add(quitter);
        menu.setCenter(scene);
        menu.setBottom(langue);
        langue.getItems().addAll(
                "Français",
                "Anglais"
        );
        langue.getSelectionModel().select("Français");

        //Tailles
        score.setPrefSize(300.0,100.0);
        quitter.setPrefSize(300.0,100.0);
        inviter.setPrefSize(300.0,100.0);

        //Polices
        Font fontTitre=new Font("Ubuntu",200);
        titre.setFont(fontTitre);
        Font fontBouton=new Font("Ubuntu",20);
        score.setFont(fontBouton);
        quitter.setFont(fontBouton);
        inviter.setFont(fontBouton);

        //Espacements et alignements
        scene.setAlignment(Pos.CENTER);
        titre.setTranslateY(-230);
        scene.setSpacing(15.0);
        menu.setAlignment(langue,Pos.BOTTOM_LEFT);
        menu.setPadding(new Insets(10));

        //Actions des boutons
        inviter.setOnAction(new ActionAffichage(this));
        score.setOnAction(new ActionScore(this,this.score));
        quitter.setOnAction(new ActionQuitter());
        langue.setOnAction(new ActionMenuAnglais(this));

        return new Scene(menu,1000,800);
    }

    /**
     * créé la scène du menu en anglais
     * @return une Scene
     */
    public Scene menuEn(){
        //Création des parties et éléments de l'interface
        Label titre=new Label("CONNECT 4");
        Button inviter=new Button("Invite a friend");
        Button score=new Button("Scores");
        Button quitter=new Button("Quit");
        BorderPane menu=new BorderPane();
        VBox scene=new VBox(5);
        ComboBox langue=new ComboBox();

        //Ajout des éléments dans les parties
        scene.getChildren().add(titre);
        scene.getChildren().add(inviter);
        scene.getChildren().add(score);
        scene.getChildren().add(quitter);
        menu.setCenter(scene);
        menu.setBottom(langue);
        langue.getItems().addAll(
                "French",
                "English"
        );
        langue.getSelectionModel().select("English");

        //Tailles
        score.setPrefSize(300.0,100.0);
        quitter.setPrefSize(300.0,100.0);
        inviter.setPrefSize(300.0,100.0);

        //Polices
        Font fontTitre=new Font("Ubuntu",200);
        titre.setFont(fontTitre);
        Font fontBouton=new Font("Ubuntu",20);
        score.setFont(fontBouton);
        quitter.setFont(fontBouton);
        inviter.setFont(fontBouton);

        //Espacements et alignements
        scene.setAlignment(Pos.CENTER);
        titre.setTranslateY(-230);
        scene.setSpacing(15.0);
        menu.setAlignment(langue,Pos.BOTTOM_LEFT);
        menu.setPadding(new Insets(10));

        //Actions des boutons
        inviter.setOnAction(new AffichageAnglais(this));
        score.setOnAction(new ActionScore(this, this.score));
        quitter.setOnAction(new ActionQuitter());
        langue.setOnAction(new ActionRetourMenu(this));

        return new Scene(menu,1000,800);
    }

////________________Invitation____________
//    public Scene invit(){
//        //Création des parties et éléments de l'interface
//        Button retour=new Button("←");
//        Label j1=new Label("Ami 1");
//        Button inviter1=new Button("✔");
//        Label j2=new Label("Ami 2");
//        Button inviter2=new Button("✔");
//        Label j3=new Label("Ami 3");
//        Button inviter3=new Button("✔");
//        VBox liste=new VBox(5);
//        HBox header=new HBox(5);
//        BorderPane scene=new BorderPane();
//        Font listeAmis=new Font("Ubuntu",20);
//
//        //Ajout des éléments dans les parties
//        header.getChildren().add(retour);
//        liste.getChildren().add(j1);
//        liste.getChildren().add(inviter1);
//        liste.getChildren().add(j2);
//        liste.getChildren().add(inviter2);
//        liste.getChildren().add(j3);
//        liste.getChildren().add(inviter3);
//        scene.setTop(header);
//        scene.setCenter(liste);
//
//        //Couleurs
//        scene.setStyle("-fx-background-color: white");
//        header.setStyle("-fx-background-color: skyblue");
//        liste.setStyle("-fx-background-color: skyblue");
//
//        //Polices
//        j1.setFont(listeAmis);
//        j2.setFont(listeAmis);
//        j3.setFont(listeAmis);
//
//        //Taille
//        retour.setPrefSize(70,60);
//        liste.setPrefWidth(50);
//
//        //Espacements et alignements
//        scene.setPadding(new Insets(10));
//        header.setPadding(new Insets(5));
//        liste.setAlignment(Pos.CENTER);
//
//        //Actions des boutons
//        inviter1.setOnAction(new ActionAffichage(this));
//        inviter2.setOnAction(new ActionAffichage(this));
//        inviter3.setOnAction(new ActionAffichage(this));
//        retour.setOnAction(new ActionRetourMenu(this));
//
//        return new Scene(scene,1000,800);
//
//    }


//_________________Jeux__________________

    /**
     * créé la scène du jeu en français
     * @return une Scene
     */
    public Scene jeuFr(){
        //Création des parties et éléments de l'interface
        Label titre=new Label("Puissance 4");
        HBox haut=new HBox(5);
        HBox header=new HBox(5);
        HBox boxJoueurs = new HBox(5);
        VBox boxMsg=new VBox(5);
        Button retour=new Button("←");
        BorderPane scene=new BorderPane();
        BorderPane centre=new BorderPane();
        File fileReveil=new File("./src/Puissance4/reveil.png");
        Image reveil=new Image(fileReveil.toURI().toString());
        ImageView viewReveil=new ImageView(reveil);
        StackPane pane = new StackPane();

        //Ajout des éléments dans les parties
        header.getChildren().add(retour);
        header.getChildren().add(titre);
        boxJoueurs.getChildren().add(lesJoueurs());
        scene.setTop(header);
        scene.setCenter(centre);
        boxMsg.getChildren().add(laMessagerieFr());
        centre.setRight(boxMsg);
        centre.setCenter(lePlateau());
        pane.getChildren().add(viewReveil);
        pane.getChildren().add(leChrono());
        haut.getChildren().add(pane);
        haut.getChildren().add(boxJoueurs);
        centre.setTop(haut);

        //Couleurs
        haut.setStyle("-fx-background-color: white");
        scene.setStyle("-fx-background-color: skyblue");

        //Tailles
        viewReveil.setPreserveRatio(true);
        viewReveil.setFitWidth(80.0);
        retour.setPrefSize(70,60);

        //Polices
        Font fontRetour=new Font("Ubuntu", 20);
        retour.setFont(fontRetour);
        Font fontTitre=new Font("Ubuntu",50);
        titre.setFont(fontTitre);

        //Espacements et alignements
        scene.setPadding(new Insets(10));
        centre.setPadding(new Insets(10));
        haut.setPadding(new Insets(1));
        boxJoueurs.setPadding(new Insets(10));
        boxJoueurs.setAlignment(Pos.CENTER_LEFT);
        boxMsg.setPadding(new Insets(12,0,0,15));
        titre.setTranslateX(450.0);
        pane.setAlignment(Pos.CENTER);
        pane.setPadding(new Insets(5));

        //Actions des boutons
        retour.setOnAction(new ActionRetourMenu(this));

        return new Scene(scene,1000,800);
    }

    /**
     * créé la scène du jeu en anglais
     * @return une Scene
     */
    public Scene jeuEn(){
        //Création des parties et éléments de l'interface
        Label titre=new Label("Connect 4");
        HBox haut=new HBox(5);
        HBox header=new HBox(5);
        HBox boxJoueurs = new HBox(5);
        VBox boxMsg=new VBox(5);
        Button retour=new Button("←");
        BorderPane scene=new BorderPane();
        BorderPane centre=new BorderPane();
        File fileReveil=new File("./src/Puissance4/reveil.png");
        Image reveil=new Image(fileReveil.toURI().toString());
        ImageView viewReveil=new ImageView(reveil);
        StackPane pane = new StackPane();

        //Ajout des éléments dans les parties
        header.getChildren().add(retour);
        header.getChildren().add(titre);
        boxJoueurs.getChildren().add(lesJoueurs());
        scene.setTop(header);
        scene.setCenter(centre);
        boxMsg.getChildren().add(laMessagerieEn());
        centre.setRight(boxMsg);
        centre.setCenter(lePlateau());
        pane.getChildren().add(viewReveil);
        pane.getChildren().add(leChrono());
        haut.getChildren().add(pane);
        haut.getChildren().add(boxJoueurs);
        centre.setTop(haut);

        //Couleurs
        haut.setStyle("-fx-background-color: white");
        scene.setStyle("-fx-background-color: skyblue");

        //Tailles
        viewReveil.setPreserveRatio(true);
        viewReveil.setFitWidth(80.0);
        retour.setPrefSize(70,60);

        //Polices
        Font fontRetour=new Font("Ubuntu", 20);
        retour.setFont(fontRetour);
        Font fontTitre=new Font("Ubuntu",50);
        titre.setFont(fontTitre);

        //Espacements et alignements
        scene.setPadding(new Insets(10));
        centre.setPadding(new Insets(10));
        haut.setPadding(new Insets(1));
        boxJoueurs.setPadding(new Insets(10));
        boxJoueurs.setAlignment(Pos.CENTER_LEFT);
        boxMsg.setPadding(new Insets(12,0,0,15));
        titre.setTranslateX(450.0);
        pane.setAlignment(Pos.CENTER);
        pane.setPadding(new Insets(5));

        //Actions des boutons
        retour.setOnAction(new ActionMenuAnglais(this));

        return new Scene(scene,1000,800);
    }

//_________________Score__________________

    /**
     * créé une scène du panneau des scores
     * @return une Scene
     */
    public Scene score(){
        //Création des parties et éléments de l'interface
        Label score = new Label("Score");
        Button retour=new Button("←");
        GridPane headerL = new GridPane();
        GridPane headerC = new GridPane();
        HBox header = new HBox(500);
        headerL.getChildren().add(retour);
        headerC.setMargin(score, new Insets(0,0,50,0));
        headerC.getChildren().add(score);
        header.getChildren().add(headerL);
        header.getChildren().add(headerC);
        BorderPane scene=new BorderPane();
        VBox gauche = new VBox();
        VBox droite = new VBox();
        HBox bas = new HBox();
        HBox haut = new HBox();
        GridPane grille = new GridPane();
        Label j1 = new Label("Joueur 1\n<Score>");
        Label j2 = new Label("Joueur 2\n<Score>");
        Label j3 = new Label("Joueur 3\n<Score>");
        Label j4 = new Label("Joueur 4\n<Score>");
        Label j5 = new Label("Joueur 5\n<Score>");
        Label j6 = new Label("Joueur 6\n<Score>");
        Label j7 = new Label("Joueur 7\n<Score>");
        Label j8 = new Label("Joueur 8\n<Score>");
        Label j9 = new Label("Joueur 9\n<Score>");
        Label j10 = new Label("Joueur 10\n<Score>");
        ColumnConstraints tailleCol=new ColumnConstraints();
        RowConstraints tailleRow=new RowConstraints();

        //Ajout des éléments dans les parties
        scene.setLeft(gauche);
        scene.setRight(droite);
        scene.setBottom(bas);
        scene.setTop(haut);
        grille.add(j1, 0, 1);
        grille.add(j2, 1, 2);
        grille.add(j3, 2, 3);
        grille.add(j4, 3, 4);
        grille.add(j5, 4, 5);
        grille.add(j6, 5, 6);
        grille.add(j7, 6, 7);
        grille.add(j8, 7, 8);
        grille.add(j9, 8, 9);
        grille.add(j10, 9, 10);
        scene.setTop(header);
        scene.setCenter(grille);

        //Couleurs
        scene.setStyle("-fx-background-color: skyblue");
        grille.setStyle("-fx-background-color :  white");

        //Tailles
        gauche.setPrefWidth(200.);
        droite.setPrefWidth(200.);
        bas.setPrefHeight(100.);
        haut.setPrefHeight(100.);
        grille.setPrefSize(200,200);
        tailleCol.setPrefWidth(100);
        tailleRow.setPrefHeight(200);
        grille.getColumnConstraints().add(tailleCol);
        grille.getRowConstraints().add(tailleRow);
        retour.setPrefSize(70,60);

        //Polices
        Font fontRetour=new Font("Ubuntu", 20);
        retour.setFont(fontRetour);
        Font fontTitre=new Font("Ubuntu",50);
        score.setFont(fontTitre);

        //Espacements et alignements
        grille.setPadding(new Insets(50));
        grille.setHgap(7); grille.setVgap(50);
        scene.setPadding(new Insets(10));


        //Actions des boutons
        retour.setOnAction(new ActionRetourMenu(this));

        return new Scene(scene,1000,800);
    }

    /**
     * change la scène actuelle
     * @param scene la nouvelle scène
     * @param titre le titre de la nouvelle scène
     */
    public void setStage(Scene scene, String titre){
        this.stage.setTitle(titre);
        this.stage.setScene(scene);
        this.stage.setFullScreen(true);
    }

    /**
     * met à jour l'affichage
     */
    public void majAffichage(){
        this.plateau = this.lePlateau();
        this.setJCourant();
    }

    /**
     * retourne le modèle du puissance 4
     * @return un Puissance4 (modèle)
     */
    public Puissance4 getModele(){ return this.puissance4; }

    /**
     * change l'affichage du joueur courant
     */
    public void setJCourant(){ this.courant=this.puissance4.getCourant().getPseudo(); }

    /**
     * initialise la vue du Puissance 4
     * @param stage le Stage du Puissance4
     */
    @Override
    public void start(Stage stage) {
        this.stage=stage;
        this.plateau=new GridPane();
        this.puissance4=new Puissance4();
        this.j1=new Joueur("Arthur",Color.RED);
        this.puissance4.ajouterJoueur(j1);
        this.j2=new Joueur("Nelson",Color.YELLOW);
        this.puissance4.ajouterJoueur(j2);
        this.puissance4.setCourant(j1);

        this.stage.setTitle("Puissance 4");
        this.stage.setScene(menuFr());
        this.stage.setFullScreen(true);
        this.stage.setFullScreenExitHint("");
        this.stage.show();
    }

    public static void main(String[]args){ launch(args); }
}
