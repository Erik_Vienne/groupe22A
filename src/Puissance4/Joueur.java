package Puissance4;


import javafx.scene.paint.Color;

class Joueur{
    private String pseudo;
    private Color couleur;

    /**
     * Constructeur du Joueur
     * @param pseudo Nom du Joueur
     * @param couleur Couleur du Joueur : Rouge ou Jaune
     */
    public Joueur(String pseudo, Color couleur){
      this.pseudo=pseudo;
      this.couleur=couleur;
    }

    /**
     *
     * @return Le nom du Joueur
     */
    public String getPseudo(){
      return this.pseudo;
    }

    /**
     *
     * @return La couleur du Joueur
     */
    public Color getCouleur(){
      return this.couleur;
    }

    /**
     * Permet de changer le nom du Joueur
     * @param nom Nouveau nom du Joueur séléctionné
     */
    public void setPseudo(String nom) {
      this.pseudo=nom;
    }

    /**
     * Permet de changer la couleur du Joueur
     * @param nvCouleur Nouvelle couleur du Joueur séléctionné
     */
    public void setCouleur(Color nvCouleur){
      this.couleur=nvCouleur;
    }

    /**
     * Utilisable pour System.out.println(...);
     * @return le Nom du Joueur
     */
    public String toString(){ return this.getPseudo(); }
}
