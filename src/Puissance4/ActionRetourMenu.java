package Puissance4;

import Puissance4.Puissance4Vue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Controller qui permet de retourner au menu en français
 */
public class ActionRetourMenu implements EventHandler<ActionEvent> {

    private Puissance4Vue p4v;

    /**
     * Constructeur du controller qui permet de retourner au menu en français
     * @param p4v
     */
    public ActionRetourMenu(Puissance4Vue p4v){
        this.p4v=p4v;
    }

    @Override
    public void handle(ActionEvent event){
        p4v.setStage(p4v.menuFr(),"Retour Menu");
    }
}
