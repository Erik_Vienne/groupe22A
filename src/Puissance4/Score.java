package Puissance4;


import java.util.ArrayList;
import java.util.List;

public class Score {
    private List<Joueur> listeJoueurs;
    private int score;
    private int maxPlaces;
    private Chronometre chrono;

    /**
     * Constructeur du Score
     * @param chrono Chronomètre qui sera utilisé pour faire le score
     */
    public Score(Chronometre chrono){
        listeJoueurs = new ArrayList<>();
        this.score = 0;
        this.chrono = chrono;
        this.maxPlaces = 10;
    }

    /**
     *
     * @return le score
     */
    public int getScore(){
        return this.score;
    }

    /**
     * Permet de calculer le score
     */
    public void setScore(){
        this.score+=this.chrono.getActionTemps().getTemps();
    }

    /**
     * Permet d'afficher le score
     * @param j le joueur
     * @return la liste des scores des joueurs
     */
    public List<Integer> scoreAffichage(Joueur j){
        List<Integer> listeScore = new ArrayList<>();
        //on ajoute les joueurs pour avoir une case en plus
        listeScore.add(this.score);
        listeJoueurs.add(j);
        int pos = listeScore.indexOf(this.score); //position actuelle su score soit dernière position de la liste
        int i = listeScore.size()-1; // longueur de la liste en comptant le 0
        if (!(pos==0)){
            while (this.score>listeScore.get(i-1) && pos == 0){
                pos = listeScore.indexOf(listeScore.get(i-1));
                listeScore.set(pos, listeScore.get(i));
                i-=1;
            }
            this.listeJoueurs.set(pos, j);
            listeScore.set(pos,this.score);
        }
        // on supprime le dernier score et le dernier joueur si il y a trop de de joueur
        if (listeScore.size()>this.maxPlaces){
            listeScore.remove(this.maxPlaces);
            this.listeJoueurs.remove(this.maxPlaces);
        }


        return listeScore;
        }
}
