package Puissance4;

import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Modèle du Puissance 4
 */
public class Puissance4{
    /**
     * liste des pions qui ont été posés
     */
    private List<Pion> listePions;
    /**
     * liste des joueurs
     */
    private List<Joueur> listeJoueurs;
    /**
     * liste des messages du chat
     */
    private List<Message> chat;
    /**
     * le joueur courant
     */
    private Joueur joueurCourant;
    /**
     * la largeur et la hauteur du plateau de jeu
     */
    private int largeur, hauteur;

    /**
     * contructeur du modèle du Puissance 4
     */
    public Puissance4(){
        this.listePions=new ArrayList<>();
        this.listeJoueurs=new ArrayList<>();
        this.chat=new ArrayList<>();
        this.largeur=6;
        this.hauteur=7;
        this.joueurCourant=null;
    }

    //Getters

    /**
     * getter de la liste de pions
     * @return une liste de pions
     */
    public List<Pion> getPions(){
        return this.listePions;
    }

    /**
     * getter de la liste des joueurs
     * @return une liste de joueurs
     */
    public List<Joueur> getListeJoueurs(){
        return this.listeJoueurs;
    }

    /**
     * getter de la messagerie
     * @return une liste de messages
     */
    public List<Message> getChat() {
        return this.chat;
    }

    /**
     * getter du joueur courant
     * @return un joueur
     */
    public Joueur getCourant() { return this.joueurCourant; }

    //Setters

    /**
     * change le joueur courant
     * @param nvJoueur un joueur
     */
    public void setCourant(Joueur nvJoueur){
        this.joueurCourant=nvJoueur;
    }

    //Ajouter

    /**
     * ajoute un pion dans la liste de pions
     * @param pion un pion
     */
    public void ajouterPion(Pion pion){
        this.listePions.add(pion);
    }

    /**
     * ajouter un joueur dans la liste de joueurs
     * @param joueur un joueur
     */
    public void ajouterJoueur(Joueur joueur){
        this.listeJoueurs.add(joueur);
    }

    /**
     * ajoute un message dans le chat
     * @param msg un message
     */
    public void ajouterMessage(Message msg){
        this.chat.add(msg);
    }

    //Méthodes de victoire

    /**
     * définit si un des joueurs a un puissance 4 en vertical
     * @param liste la liste des pions posés
     * @return un booléen
     */
    public boolean victoireVerticale(List<Pion> liste){
        for(Pion pion1 : liste) {
            Pion pion2 = new Pion(pion1.getCouleur(), pion1.getPosX(), pion1.getPosY() + 1);
            if (liste.contains(pion2)) {
                Pion pion3 = new Pion(pion1.getCouleur(), pion1.getPosX(), pion2.getPosY() + 2);
                if (liste.contains(pion3)) {
                    Pion pion4 = new Pion(pion1.getCouleur(), pion1.getPosX(), pion3.getPosY() + 3);
                    if (liste.contains(pion4)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * définit si un joueur a un puissance 4 en horizontal
     * @param liste la liste des pions posés
     * @return un booléen
     */
    public boolean victoireHorizontale(List<Pion> liste){
        for(Pion pion1 : liste) {
            Pion pion2 = new Pion(pion1.getCouleur(), pion1.getPosX()+1, pion1.getPosY());
            if (liste.contains(pion2)) {
                Pion pion3 = new Pion(pion1.getCouleur(), pion1.getPosX()+2, pion1.getPosY());
                if (liste.contains(pion3)) {
                    Pion pion4 = new Pion(pion1.getCouleur(), pion3.getPosX()+3, pion1.getPosY());
                    if (liste.contains(pion4)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * définit si un joueur a un puissance 4 en diagonale
     * @param liste la liste des pions posés
     * @return un booléen
     */
    public boolean victoireDiagonale(List<Pion> liste){
        for(Pion pion1 : liste) {
            Pion pion2 = new Pion(pion1.getCouleur(), pion1.getPosX()+1, pion1.getPosY() + 1);
            if (liste.contains(pion2)) {
                Pion pion3 = new Pion(pion1.getCouleur(), pion1.getPosX()+2, pion1.getPosY() + 2);
                if (liste.contains(pion3)) {
                    Pion pion4 = new Pion(pion1.getCouleur(), pion1.getPosX()+3, pion1.getPosY() + 3);
                    if (liste.contains(pion4)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * définit di un de joueurs a gagné
     * @return un booléen
     */
    public boolean gagne(){
        return this.victoireDiagonale(this.getPions())
                ||this.victoireHorizontale(this.getPions())
                ||this.victoireVerticale(this.getPions());
    }

    /**
     * change le joueur courant
     */
    public void changeCourant(){
        if (this.getCourant() == this.getListeJoueurs().get(0)) {
            this.setCourant(this.getListeJoueurs().get(1));
        } else {
            this.setCourant(this.getListeJoueurs().get(0));
        }
    }
}
