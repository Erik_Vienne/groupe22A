package Puissance4;

import javafx.scene.paint.Color;

class Pion{
  private int posX, posY;
  private Color couleur;

  /**
   * Constructeur du Pion
   * @param couleur Couleur du pion
   * @param posX Position en X du pion
   * @param posY Position en Y du pion
   */
  public Pion(Color couleur, int posX, int posY){
    this.posX=posX;
    this.posY=posY;
    this.couleur=couleur;
  }

  /**
   *
   * @return la position en X du pion
   */
  public int getPosX(){ return this.posX; }

  /**
   *
   * @return la position en Y du pion
   */
  public int getPosY(){ return this.posY; }

  /**
   *
   * @return la couleur du pion
   */
  public Color getCouleur(){ return this.couleur; }

  /**
   * Permet de chnger la position en X du pion
   * @param pos nouvelle position en X
   */
  public void setPosX(int pos){ this.posX=pos; }

  /**
   * Permet de chnger la position en Y du pion
   * @param pos nouvelle position en X
   */
  public void setPosY(int pos){ this.posY=pos; }

  /**
   * Redéfinition de la méthode equals
   * @param o un objet caster en pion par la suite
   * @return un boolean si deux pions ont la même position en X et en Y
   */
  @Override
  public boolean equals(Object o){
    Pion pion=(Pion) o;
    if(this.getPosX()==pion.getPosX() && this.getPosY()==pion.getPosY()){
      return true;
    }
    return false;
  }

  /**
   * Utilisable pour System.out.println(...);
   * @return la couleur la position en X et en Y du pion
   */
  public String toString(){
    if(this.getCouleur().equals(Color.RED)) {
        return "Rouge, " + this.getPosX() + ", " + this.getPosY();
    }
    else{
        return "Jaune, " + this.getPosX() + ", " + this.getPosY();
    }
  }
}
