package Puissance4;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

/**
 * Controller qui permet de poser un pion
 */
public class ActionPoserPion implements EventHandler<ActionEvent> {
    private Puissance4Vue p4v;
    private Puissance4 puissance4;

    /**
     * Constructeur du controller qui permet de poser un pion
     * @param p4v
     * @param puissance4
     */
    public ActionPoserPion(Puissance4Vue p4v, Puissance4 puissance4){
        this.p4v=p4v;
        this.puissance4=puissance4;
    }

    @Override
    public void handle(ActionEvent event){
        Button boutonActif = (Button)event.getSource();
        int i=Integer.parseInt(boutonActif.getText()); //on récupère le numéro du bouton
        int recupJ=0; //on récupère la ligne du pion le plus haut de la colonne
        for(int j=6 ; j>=0 ; j--) {
            Pion pion = new Pion(puissance4.getCourant().getCouleur(), i-1, j);
            recupJ=j;
            if(this.puissance4.gagne()){
                this.p4v.getChrono().stop();
                Alert fin=new Alert(Alert.AlertType.INFORMATION);
                fin.setTitle("FIN DE PARTIE");
                fin.setHeaderText("VAINQUEUR : "+this.puissance4.getCourant().getPseudo());
                fin.setContentText("Bravo à lui/elle !");
                this.p4v.setStage(this.p4v.menuFr(),"Puissance 4");
                fin.show();
            }
            if(j==0){ // si il n'y a plus de place dans la colonne, envoie un message d'erreur
                Alert pasDePlace=new Alert(Alert.AlertType.INFORMATION);
                pasDePlace.setTitle("ATTENTION");
                pasDePlace.setHeaderText("PLUS DE PLACE DANS CETTE COLONNE");
                pasDePlace.setContentText("Veuillez en choisir une autre");
                pasDePlace.show();
            }
            else if (!this.puissance4.getPions().contains(pion)) {
                this.puissance4.ajouterPion(pion);
                break;
            }
        }

        if(recupJ!=0) { //si il reste de la place, le pion est posé, le chrono réinitialisé et le joueur courant est changé
            this.puissance4.changeCourant();
            this.p4v.getChrono().resetTime();
        }
        this.p4v.setJCourant();
        this.p4v.majAffichage();
    }
}
