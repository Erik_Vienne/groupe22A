package Puissance4;

import java.util.Date;

class Message {
    private String msg;
    private Date date;
    private String expediteur, destinataire;

    /**
     * Constructeur de Message
     * @param expediteur Nom de l'expéditeur
     * @param destinataire Nom du destinateur
     * @param contenu le message à envoyé
     */
    public Message(String expediteur, String destinataire, String contenu){
        this.msg=contenu;
        this.date = date;
        this.expediteur = expediteur;
    }

    /**
     * Permet de dater les messages
     * @return la date
     */
    public long getDate(){
        return new java.util.Date().getTime();
    }

    /**
     * Permet de modifier le message
     * @param m nouveau message
     */
    public void setMessage(String m) {
        this.msg=m;
    }

    /**
     * Permet de changer le nom de l'expéditeur
     * @param e le nom du nouvel expéditeur
     */
    public void setNom(String e) {
        this.expediteur = e;
    }
}
