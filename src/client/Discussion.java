package client;

import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import java.io.ByteArrayInputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Onglet discussion
 */
public class Discussion {
    /**
     * Créé la page de chat avec un joueur
     * @param app L'application du client
     * @param ami L'ami avec qui on parle
     * @return La scene qui correspond à la page demandée
     */
    public static Scene setDiscussion(Client app, Utilisateur ami){
        BorderPane cont = new BorderPane();
        BorderPane mid = new BorderPane();



        GridPane chat = new GridPane();
        chat.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        ColumnConstraints col = new ColumnConstraints();
        col.setPercentWidth(50);

        chat.getColumnConstraints().add(col);

        ColumnConstraints c1 = new ColumnConstraints();
        c1.setPercentWidth(100);

        List<Message> listeMessages = new ArrayList<>();
        try{
            listeMessages = BibliMySQLClient.getConversation(app.mySQL, app.getUtilisateur(), ami);
        }
        catch (SQLException e){
            System.out.println("pb");
        }

        Label chatMessage;
        for (int i = 0; i<listeMessages.size();i++) {
            Message msg = listeMessages.get(i);
            chatMessage = new Label(msg.getContenu());
            chatMessage.setWrapText(true);
            if(app.getUtilisateur().getId() == msg.getEnv().getId()){
                GridPane.setHalignment(chatMessage,  HPos.RIGHT);
                chat.add(chatMessage,1, i);
            }
            else{
                GridPane.setHalignment(chatMessage,  HPos.LEFT);
                chat.add(chatMessage,0, i);
            }

        }

        ScrollPane scroll = new ScrollPane(chat);
        scroll.setFitToWidth(true);
        scroll.setVvalue(1.0);

        TextField envoyer = new TextField();
        envoyer.setOnKeyPressed((KeyEvent ke) ->{
            if(ke.getCode().equals(KeyCode.ENTER) && !envoyer.getText().equals("")){
                ActionEnvoyerMessage handler = new ActionEnvoyerMessage(app, ami, envoyer.getText());
                handler.handle(new ActionEvent());
            }
        });
        envoyer.setPromptText("Saisissez votre message");
        mid.setTop(Template.templateMenus("Messages", app));
        mid.setCenter(scroll);
        mid.setBottom(envoyer);
        cont.setCenter(mid);
        cont.setRight(Template.templateJoueurAmis(app, "Classique"));
        return new Scene(cont,app.HAUTEUR,app.LARGEUR);
    }
}
