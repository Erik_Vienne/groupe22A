package client;

import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

import java.sql.SQLException;

/**
 * Administration des statistiques
 */
public class AdministrationStats {

    /**
     * Créé la page d'administration des stats
     * @param app L'application du client
     * @return La scene qui correspond à la page demandée
     */
    public static Scene setAdministrationStats(Client app) {
        BorderPane cont = new BorderPane();
        Label texte = null;
        try{
            texte = new Label(BibliMySQLClient.listeTousJoueurs(app.mySQL).size() + " joueurs inscrits");
        }
        catch (SQLException e){
            System.out.println("pb");
        }
        BorderPane mid = new BorderPane();
        mid.setTop(Template.templateMenusAdmin("Statistiques", app));
        mid.setCenter(texte);
        cont.setCenter(mid);
        cont.setRight(Template.templateJoueurAmis(app, "Administration"));
        return new Scene(cont, app.HAUTEUR, app.LARGEUR);
    }
}
