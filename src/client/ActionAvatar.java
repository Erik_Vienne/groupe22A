package client;

import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class ActionAvatar implements EventHandler<MouseEvent> {
    Client app;
    ImageView img;
    FileChooser fc;
    List<String> types;

    public ActionAvatar(Client app, ImageView img){
        this.app=app;
        this.img=img;
        this.fc = new FileChooser();
        this.fc.setInitialDirectory(new File("."));
        this.types = Arrays.asList("image/png","image/jpeg");
    }
    @Override
    public void handle(MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount()==2) {
            File f = this.fc.showOpenDialog(null);
            try {
                System.out.println(Files.probeContentType(f.toPath()));
                if (types.contains(Files.probeContentType(f.toPath()))) {
                    BibliMySQLClient.setAvatar(app.mySQL, app.getUtilisateur(), Files.readAllBytes(f.toPath()));
                    app.setUtilisateur(Utilisateur.instancierParID(app.mySQL, app.getUtilisateur().getId()));
                    img.setImage(new Image(new ByteArrayInputStream(Files.readAllBytes(f.toPath()))));
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }
}
