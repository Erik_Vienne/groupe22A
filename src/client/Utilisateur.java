package client;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Une classe du modèle représentant un utilisateur de l'application
 */
public class Utilisateur {
    private int id;
    private String pseudo, mail;
    private boolean actif, admin;
    private byte[] avatar;

    private Utilisateur(int id, String pseudo, String mail, boolean actif, boolean admin, byte[] avatar){
        this.id=id;
        this.pseudo=pseudo;
        this.mail=mail;
        this.actif=actif;
        this.admin=admin;
        this.avatar=avatar;
    }

    /**
     * Utilise la base de données pour instancier un utilisateur selon son ID
     * @param c une ConnectionMySQL à la BD
     * @param id l'ID de l'utilisateur à instancier
     * @return l'utilisateur instancié
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     */
    public static Utilisateur instancierParID(ConnexionMySQL c, int id) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        ResultSet rs = c.createStatement().executeQuery("select * from UTILISATEUR where idUt="+id);
        if (!rs.next()) {
            throw new SQLException();
        }
        byte[] avatar = rs.getBytes("avatarUt");
        if (avatar==null){
            ResultSet avDef = c.createStatement().executeQuery("select * from AVDEF");
            avDef.next();
            avatar = avDef.getBytes(1);
        }
        return new Utilisateur(id, rs.getString("pseudoUt"), rs.getString("emailUt"), rs.getBoolean("activeUt"), rs.getBoolean("adminUt"), avatar);

    }

    /**
     * Utilise la base de données pour instancier un utilisateur selon son pseudo
     * @param c une ConnectionMySQL à la BD
     * @param pseudo le pseudo de l'utilisateur à instancier
     * @return l'utilisateur instancié
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     */
    public static Utilisateur instancierParPseudo(ConnexionMySQL c, String pseudo) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        PreparedStatement ps = c.prepareStatement("select * from UTILISATEUR where pseudoUt=?");
        ps.setString(1, pseudo);
        ResultSet rs = ps.executeQuery();
        if (!rs.next())
            throw new SQLException();
        byte[] avatar = rs.getBytes("avatarUt");
        if (avatar==null){
            ResultSet avDef = c.createStatement().executeQuery("select * from AVDEF");
            avDef.next();
            avatar = avDef.getBytes(1);
        }
        return new Utilisateur(rs.getInt("idUt"), rs.getString("pseudoUt"), rs.getString("emailUt"), rs.getBoolean("activeUt"), rs.getBoolean("adminUt"), avatar);

    }

    /**
     * Renvoie l'avatar de l'utilisateur
     * @return un byte array correspondant à une image
     */
    public byte[] getAvatar(){
        return avatar;
    }

    /**
     * Renvoie si l'utilisateur est actif
     * @return true si l'utilisateur est actif, false sinon
     */
    public boolean estActif() {
        return actif;
    }

    /**
     * Renvoie si l'utilisateur est admin
     * @return true si l'utilisateur est admin, false sinon
     */
    public boolean estAdmin() {
        return admin;
    }

    /**
     * Renvoie l'ID de l'utilisateur
     * @return l'ID de l'utilisateur
     */
    public int getId() {
        return id;
    }

    /**
     * Renvoie l'adresse mail de l'utilisateur
     * @return l'adresse mail de l'utilisateur
     */
    public String getMail() {
        return mail;
    }

    /**
     * Renvoie le pseudo de l'utilisateur
     * @return le pseudo de l'utilisateur
     */
    public String getPseudo() {
        return pseudo;
    }

    @Override
    public String toString() {
        return "Utilisateur "+id+":"+pseudo+" ("+mail+", "+(actif?"activé":"desactivé")+", "+(admin?"admin":"normal")+")";
    }
}
