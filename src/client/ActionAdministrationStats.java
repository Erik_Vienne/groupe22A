package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Action du bouton pour acceder à l'administration des stats
 */
public class ActionAdministrationStats implements EventHandler<ActionEvent> {
    private Client app;

    /**
     * Constructeur du controleur de l'administration des stats
     * @param app L'application du client
     */
    public ActionAdministrationStats(Client app){
        this.app = app;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        app.setStage(AdministrationStats.setAdministrationStats(app), "Duel sur la toile - Administration");
    }
}
