package client;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.ByteArrayInputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Onglet des messages
 */
public class Messages {
    /**
     * Créé la page des messages
     * @param app L'application du client
     * @return La scene qui correspond à la page demandée
     */
    public static Scene setMessages(Client app) {
        BorderPane cont = new BorderPane();
        BorderPane mid = new BorderPane();

        List<Utilisateur> mesAmis = new ArrayList<>();
        VBox listeAmis = new VBox();
        ScrollPane sp = new ScrollPane();


        try{
            mesAmis = BibliMySQLClient.listeAmis(app.mySQL, app.getUtilisateur());
        }
        catch(SQLException e){
            System.out.println("pb");
        }
        for (Utilisateur user : mesAmis) {
            HBox ami = new HBox();
            ImageView img = new ImageView();
            img.setImage(new Image(new ByteArrayInputStream(user.getAvatar())));
            img.setFitWidth(60);
            img.setFitHeight(60);
            Label pseudo = new Label(user.getPseudo());

            ami.setPadding(new Insets(10));
            ami.getChildren().addAll(img, pseudo);
            ami.setOnMouseClicked(new ActionMessageToDiscussion(app, user));
            ami.setOnMouseEntered(new EventHandler<MouseEvent>() {
                public void handle(MouseEvent me) {
                    ami.setStyle("-fx-background-color: #CCCCCC;");
                }
            });

            ami.setOnMouseExited(new EventHandler<MouseEvent>() {
                public void handle(MouseEvent me) {
                    ami.setStyle("");
                }
            });
            listeAmis.getChildren().add(ami);
        }


        sp.setContent(listeAmis);
        mid.setTop(Template.templateMenus("Messages", app));
        mid.setCenter(sp);
        cont.setCenter(mid);
        cont.setRight(Template.templateJoueurAmis(app, "Classique"));
        return new Scene(cont, app.HAUTEUR,app.LARGEUR);
    }
}
