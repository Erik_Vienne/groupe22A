package client;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Une classe du modèle représentant un message privé entre deux utilisateurs
 */
public class Message {
    private int id;
    private Utilisateur env, dest;
    private java.util.Date date;
    private String contenu;

    private Message(int id, Utilisateur env, Utilisateur dest, Date date, String contenu){
        this.id = id;
        this.env = env;
        this.dest = dest;
        this.date = date;
        this.contenu = contenu;
    }

    /**
     * Utilise la base de données pour instancier un message selon son ID
     * @param c une ConnectionMySQL à la BD
     * @param id l'ID du message à instancier
     * @return le message instancié
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     */
    public static Message instancierParID(ConnexionMySQL c, int id) throws SQLException {
        ResultSet rs = c.createStatement().executeQuery("select * from MESSAGE where idMsg="+id);
        rs.next();
        int idEnv = rs.getInt("idEnv");
        int idDest = rs.getInt("idDest");
        return new Message(id, Utilisateur.instancierParID(c,idEnv), Utilisateur.instancierParID(c, idDest), rs.getDate("dateMsg"), rs.getString("contenuMsg"));
    }

    /**
     * Renvoie l'id du message
     * @return l'id du message
     */
    public int getId() {
        return id;
    }

    /**
     * Renvoie l'utilisateur ayant envoyé le message
     * @return l'utilisateur ayant envoyé le message
     */
    public Utilisateur getEnv() {
        return env;
    }

    /**
     * Renvoie l'utilisateur devant recevoir le message
     * @return l'utilisateur devant recevoir le message
     */
    public Utilisateur getDest() {
        return dest;
    }

    /**
     * Renvoie la date d'envoi du message
     * @return la date d'envoi du message
     */
    public java.util.Date getDate() {
        return date;
    }

    /**
     * Renvoie le contenu du message
     * @return le contenu du message
     */
    public String getContenu() {
        return contenu;
    }

    @Override
    public String toString() {
        return "Message (id:"+id+", env:"+env.getPseudo()+", dest:"+dest.getPseudo()+", date:"+date+"): "+contenu;
    }
}
