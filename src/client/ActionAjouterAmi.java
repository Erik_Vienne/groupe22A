package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextInputDialog;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Action du bouton pour ajouter un ami
 */
public class ActionAjouterAmi implements EventHandler<ActionEvent> {
    Client app;
    Utilisateur AmiAAjouter;

    /**
     * Constructeur du controleur pour ajouter un ami
     * @param app L'application du client
     */
    public ActionAjouterAmi (Client app){
        this.app = app;
    }

    /**
     * Fait apparaitre une pop-up pour écrire le nom de la personne qui doit être ajoutée et envoie la demande
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        TextInputDialog ajouterAmi = new TextInputDialog();
        ajouterAmi.setTitle("Ajouter un ami");
        ajouterAmi.setHeaderText("Qui voulez-vous ajouter ?");
        ajouterAmi.setContentText("Nom de l'utilisateur :");


        Optional<String> result = ajouterAmi.showAndWait();
        if (result.isPresent()) {
            try{
                AmiAAjouter = Utilisateur.instancierParPseudo(app.mySQL, result.get());
                BibliMySQLClient.envoyerDemandeAmi(app.mySQL, AmiAAjouter, app.getUtilisateur());
            }
            catch (SQLException e){
                System.out.println("pb");
            }
        }
    }
}
