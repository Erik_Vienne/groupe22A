package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Action pour acceder aux nouveautés
 */
public class ActionNouveautes implements EventHandler<ActionEvent> {
    private Client app;

    /**
     * Constructeur du controleur des nouveautes
     * @param app L'application du client
     */
    public ActionNouveautes(Client app){
        this.app = app;
    }

    /**
     * Met à jour la fenêtre
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        app.setStage(Nouveautes.setNouveautes(app),"Duel sur la toile - Nouveautés" );
    }
}
