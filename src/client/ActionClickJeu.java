package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;

import java.io.File;
import java.io.InputStream;
import java.util.Optional;

/**
 * Action pour acceder à un jeu
 */
public class ActionClickJeu implements EventHandler<ActionEvent> {
    private Client app;
    private Partie partie;
    public ActionClickJeu(Client app, Partie partie){
        this.app=app;
        this.partie = partie;
    }

    /**
     * Fait apparaitre une pop-up lors d'un double click sur le nom d'un jeu
     * Les fonctions d'action sont : Acceder à ses parties, jouer ou annuler
     */
    @Override
    public void handle(ActionEvent event) {
        String jeuSelect = partie.getJeu().getNom();


        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(jeuSelect);
        alert.setHeaderText("Que voulez-vous faire ?");
//            alert.setContentText("Choose your option.");

        ButtonType buttonTypeJouer = new ButtonType("Jouer");
        ButtonType buttonTypeAnnuler = new ButtonType("Annuler", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonTypeJouer, buttonTypeAnnuler);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeJouer) {
            try {
//                Jeu j = Jeu.instancierParNom(app.mySQL, jeuSelect);
//                j.telechargerJeu();
                if (jeuSelect.equals("Morpion"))
                    new morpion.MorpionAccueil().demarrer(partie.getId(), app.getUtilisateur().getId());

                if (jeuSelect.equals("Mastermind")||jeuSelect.equals("Puissance 4")) {
                    ProcessBuilder pb = new ProcessBuilder("/usr/bin/java", "-jar", "jars/" + jeuSelect + ".jar");
                    pb.directory(new File("."));
                    Process p = pb.start();
                    p.waitFor();
                }
//                InputStream in = p.getInputStream();
//                byte b[]=new byte[in.available()];
//                in.read(b,0,b.length);
//                System.out.println(new String(b));
//
//                InputStream err = p.getErrorStream();
//                byte c[]=new byte[err.available()];
//                err.read(c,0,c.length);
//                System.err.println(new String(c));
            } catch (Exception e){e.printStackTrace();}
        } else {
            // ... Annuler
        }
    }
}

