package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ActionLogin implements EventHandler<ActionEvent> {
    private Client app;

    public ActionLogin(Client app){
        this.app = app;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        String pseudo = app.getLoginPseudo();
        String motDePasse = app.getLoginMdp();
        try {
            ConnexionMySQL c = app.mySQL;
            if (BibliMySQLClient.mdpLogin(c, pseudo, motDePasse)){
                Utilisateur u = Utilisateur.instancierParPseudo(c, pseudo);
                app.setUtilisateur(u);
                if (!u.estActif()){
                    app.setLoginMessage("Compte désactivé par un administrateur");
                } else {
                    app.setStage(Nouveautes.setNouveautes(app), "Duel sur la toile - Nouveautés");
                }
            } else {
                app.setLoginMessage("Pseudo ou mot de passe non reconnu");
            }
        } catch (NullPointerException | NoSuchAlgorithmException e){
            e.printStackTrace();
            app.setLoginMessage("Votre installation de Java\nn'est pas supportée");
        } catch (SQLException e){
            System.out.println(e);
            app.setLoginMessage("Connexion au serveur impossible");
        }
    }

}
