package client;


import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;

import java.sql.SQLException;
import java.util.Optional;

/**
 * Action pour administrer les joueurs
 */
public class ActionClickAdministrationJoueur implements EventHandler<MouseEvent> {
    String nomJoueur;
    Client app;
    boolean actif;

    /**
     * Constructeur du controleur d'administration des joueur
     * @param nom Le joueur à administrer
     * @param app L'application du client
     */
    public ActionClickAdministrationJoueur(String nom, Client app){
        nomJoueur = nom;
        this.app = app;
    }

    /**
     * Fait apparaitre une pop-up lors d'un double click sur le nom d'un joueur
     * Les fonctions d'administration sont : Activer un joueur, le désactiver ou annuler
     */
    @Override
    public void handle(MouseEvent click) {

        if (click.getClickCount() == 2) {
            nomJoueur = app.list.getSelectionModel().getSelectedItem().toString();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Modification du joueur");
            try{
                if(Utilisateur.instancierParPseudo(app.mySQL, nomJoueur).estActif()){
                    actif = true;
                    alert.setHeaderText("Desactiver " + nomJoueur + " ?");
                }
                else{
                    actif = false;
                    alert.setHeaderText("Activer " + nomJoueur + " ?");
                }
            }
            catch(SQLException e){

            }


            Optional<ButtonType> result = alert.showAndWait();
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
            if (result.get() == ButtonType.OK){
                try {
                    if(!actif) {

                        BibliMySQLClient.setActif(app.mySQL, Utilisateur.instancierParPseudo(app.mySQL, nomJoueur), true);
                        alert2.setTitle("Activation du joueur");
                        alert2.setHeaderText("Joueur activé");
                        alert2.show();
                    }
                    else{

                        BibliMySQLClient.setActif(app.mySQL, Utilisateur.instancierParPseudo(app.mySQL, nomJoueur), false);
                        alert2.setTitle("Desactivation du joueur");
                        alert2.setHeaderText("Joueur Desactivé");
                        alert2.show();
                    }
                }
                catch (SQLException e){
                    System.out.println("Problème");
                }
            }

            else {
                // ... Annuler
                System.out.println("action annulée");
            }
        }
    }
}