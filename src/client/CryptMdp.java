package client;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CryptMdp {
//    public static void main(String[] args) {
//        try {
//            System.out.println(CryptMdp.encode("user1", "password"));
//            System.out.println(CryptMdp.encode("user2", "password"));
//
//
//
//            System.out.println(Arrays.equals(encodedPass, encoder.digest(System.console().readLine().getBytes(StandardCharsets.UTF_8))));
//
//        } catch (NoSuchAlgorithmException e){
//            System.out.println("Algorithm not found");
//        }
//    }
    public static String encode(String username, String password) throws NoSuchAlgorithmException{
        MessageDigest encoder = MessageDigest.getInstance("SHA-256");
        byte[] encodedUn = encoder.digest(username.getBytes(StandardCharsets.UTF_8));
        return CryptMdp.byteArrayToString(encoder.digest((CryptMdp.byteArrayToString(encodedUn)+password).getBytes(StandardCharsets.UTF_8)));
    }

    private static String byteArrayToString(byte[] bytes){
        char[] chars = new char[bytes.length];
        for (int i=0; i<bytes.length; i++){
            chars[i] = (char)bytes[i];
        }
        return String.valueOf(chars);
    }
}
