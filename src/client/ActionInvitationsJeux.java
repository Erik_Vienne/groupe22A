package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Action pour acceder à la page des invitations de jeu
 */
public class ActionInvitationsJeux implements EventHandler<ActionEvent> {
    private Client app;

    /**
     * Constructeur du controleur des demandes de jeu
     * @param app L'application du client
     */
    public ActionInvitationsJeux(Client app) {
        this.app = app;
    }

    /**
     * Met à jour la fenêtre
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        app.setStage(Invitations.setInvitations(app, Invitations.JEUX), "Duel sur la toile - Invitations");
    }
}