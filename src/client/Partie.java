package client;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Partie {
    private int id;
    private Jeu jeu;
    private boolean fini;
    private Utilisateur adversaire;

    private Partie(int id, Jeu jeu, Utilisateur adversaire, boolean fini){
        this.id=id;
        this.jeu=jeu;
        this.adversaire=adversaire;
        this.fini=fini;
    }

    public static Partie instancierParID(ConnexionMySQL c, int id, Utilisateur u) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        ResultSet rs = c.createStatement().executeQuery("select PARTIE.idPa, idJeu, finiePa, P1.idUt from PARTIE natural join PARTICIPER P1 join PARTICIPER P2 on P1.idPa=P2.idPa where P1.idUt!=P2.idUt and P2.idUt="+u.getId()+" and PARTIE.idPa="+id);
        rs.next();
        return new Partie(id, Jeu.instancierParID(c, rs.getInt("idJeu")), Utilisateur.instancierParID(c, rs.getInt("P1.idUt")), rs.getBoolean("finiePa"));
    }

    public int getId() {
        return id;
    }

    public Jeu getJeu() {
        return jeu;
    }

    public Utilisateur getAdversaire() {
        return adversaire;
    }

    public boolean isFini() {
        return fini;
    }
}
