package client;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

/**
 * Action pour acceder à une conversation
 */
public class ActionMessageToDiscussion  implements EventHandler<MouseEvent> {
    Client app;
    Utilisateur parlerAvec;

    /**
     * Constructeur du controleur pour acceder à une conversation
     * @param app L'application du client
     * @param parlerAvec L'utilisateur avec qui l'on souhaite parler
     */
    public ActionMessageToDiscussion(Client app, Utilisateur parlerAvec){
        this.app = app;
        this.parlerAvec = parlerAvec;
    }

    /**
     * Met à jour la fenêtre lors d'un double click sur l'utilisateur avec qui l'on souhaite parler
     */
    @Override
    public void handle(MouseEvent click) {

        if (click.getClickCount() == 2) {
            app.setStage(Discussion.setDiscussion(app, parlerAvec), "Duel sur la toile - Parler avec " + parlerAvec.getPseudo());
        }
    }
}

