package client;

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Une bibliothèque contenant diverses fonctions permettant au client d'intéragir avec la base de données.
 */
public class BibliMySQLClient {
    public static List<String> getNomsColonnes(ResultSet rs) throws SQLException{
        ResultSetMetaData rsmd = rs.getMetaData();
        List<String> l = new ArrayList<>();
        for (int i = 1; i <= rsmd.getColumnCount(); i++){
            l.add(rsmd.getColumnName(i));
        }
        return l;
    }

    /**
     * Renvoie une ConnexionMySQL connectée à la base de données.
     * @return une ConnexionMySQL connectée à la base de données, null en cas d'erreur.
     */
    public static ConnexionMySQL getConnexion(){
        String serv = "servinfo-db";
        try {
            ConnexionMySQL c = new ConnexionMySQL();
            c.connecter(serv, "dbsleroy", "sleroy", "good_password");
            return c;
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e);
            return null;
        }

    }

    /**
     * Vérifie si le pseudo et le mot de passe correspondent aux données de la BD.
     * @param c une ConnexionMySQL connectée à la BD
     * @param pseudo le pseudo de l'utilisateur voulant se connecter
     * @param mdp le mot de passe proposé
     * @return true si la combinaison correspond, false sinon
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     * @throws NoSuchAlgorithmException si l'algorithme sha256 est inutilisable
     */
    public static boolean mdpLogin(ConnexionMySQL c, String pseudo, String mdp) throws SQLException, NoSuchAlgorithmException{
        PreparedStatement s = c.prepareStatement("select * from UTILISATEUR where pseudoUt=?");
        s.setString(1, pseudo);
        ResultSet rs = s.executeQuery();
        rs.next();
        return rs.getString("mdpUt").equals(CryptMdp.encode(pseudo, mdp));
    }

    /**
     * Renvoie la plus grande ID d'utilisateur de la BD.
     * @param c une ConnexionMySQL à la BD.
     * @return la plus grande ID d'utilisateur de la BD.
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     */
    public static int getMaxID(ConnexionMySQL c) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        ResultSet rs = c.createStatement().executeQuery("select max(idUt) maxID from UTILISATEUR");
        if(rs.next())
            return rs.getInt("maxID");
        return 0;
    }

    /**
     * Vérifie si le pseudo est disponible
     * @param c une ConnexionMySQL à la BD
     * @param pseudo le pseudo à vérifier
     * @return true si le pseudo est disponible, false sinon
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     */
    public static boolean pseudoLibre(ConnexionMySQL c, String pseudo) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        PreparedStatement ps = c.prepareStatement("select exists(select * from UTILISATEUR where pseudoUt=?) pseudoUtilise");
        ps.setString(1, pseudo);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return !rs.getBoolean("pseudoUtilise");
    }

    /**
     * Vérifie si l'adresse mail est inutilisé
     * @param c une ConnexionMySQL à la BD
     * @param mail l'adresse mail à vérifier
     * @return true si l'adresse mail est disponible, false sinon
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     */
    public static boolean mailLibre(ConnexionMySQL c, String mail) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        PreparedStatement ps = c.prepareStatement("select exists(select * from UTILISATEUR where emailUt=?) mailUtilise");
        ps.setString(1, mail);
        ResultSet rs = ps.executeQuery();
        rs.next();
        return !rs.getBoolean("mailUtilise");
    }

    /**
     * Insère un utilisateur dans la base de données.
     * @param c une ConnexionMySQL à la BD
     * @param id l'id de l'utilisateur à créer
     * @param pseudo le pseudo de l'utilisateur à créer
     * @param mail l'adresse mail de l'utilisateur à créer
     * @param mdp le mot de passe de l'utilisateur à créer
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     * @throws NoSuchAlgorithmException si l'algorithme sha256 est inutilisable
     */
    public static void creerUtilisateur(ConnexionMySQL c, int id, String pseudo, String mail, String mdp) throws SQLException, NoSuchAlgorithmException {
        PreparedStatement s = c.prepareStatement("insert into UTILISATEUR values (?, ?, ?, ?, null, '1', '0')");
        s.setInt(1, id);
        s.setString(2, pseudo);
        s.setString(3, mail);
        s.setString(4, CryptMdp.encode(pseudo, mdp));
        s.execute();
    }

    /**
     * Recherche dans la liste des utilisateurs ceux dont le pseudo possède la sous-chaine
     * @param c une ConnexionMySQL à la BD
     * @param recherche la sous-chaine recherchée
     * @return une liste d'utilisateurs dont le pseudo contient recherche
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     */
    public static List<Utilisateur> rechercheParNom(ConnexionMySQL c, String recherche) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        PreparedStatement s = c.prepareStatement("select idUt from UTILISATEUR where pseudoUt like ? order by pseudoUt");
        s.setString(1, "%"+recherche+"%");
        ResultSet rs = s.executeQuery();
        List<Utilisateur> l = new ArrayList<>();
        while(rs.next()){
            l.add(Utilisateur.instancierParID(c, rs.getInt("idUt")));
        }
        return l;
    }

    /**
     * Renvoie la liste complète des utilisateurs
     * @param c une ConnexionMySQL à la BD
     * @return la liste complète des utilisateurs
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     */
    public static List<Utilisateur> listeTousJoueurs(ConnexionMySQL c) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        ResultSet rs = c.createStatement().executeQuery("select idUt from UTILISATEUR order by pseudoUt");
        List<Utilisateur> l = new ArrayList<>();
        while(rs.next()){
            l.add(Utilisateur.instancierParID(c, rs.getInt("idUt")));
        }
        return l;
    }

    /**
     * Active ou désactive un utilisateur
     * @param c une ConnexionMySQL à la BD
     * @param u l'utilisateur à activer ou désactiver
     * @param a true si l'utilisateur doit être activé, false s'il doit être désactivé
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     */
    public static void setActif(ConnexionMySQL c, Utilisateur u, boolean a) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        if (!u.estAdmin()) {
            PreparedStatement ps = c.prepareStatement("update UTILISATEUR set activeUt=? where idUt=?");
            ps.setBoolean(1, a);
            ps.setInt(2, u.getId());
            ps.executeUpdate();
        }
    }

    /**
     * Modifie ou ajoute un avatar à l'utilisateur
     * @param c une ConnexionMySQL à la BD
     * @param u l'utilisateur dont l'avatar doit être changé
     * @param b un byte array correspondant à une image
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     */
    public static void setAvatar(ConnexionMySQL c, Utilisateur u, byte[] b) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        PreparedStatement ps = c.prepareStatement("update UTILISATEUR set avatarUt=? where idUt=?");
        Blob blob = c.createBlob();
        blob.setBytes(1, b);
        ps.setBlob(1, blob);
        ps.setInt(2, u.getId());
        ps.executeUpdate();
    }

    /**
     * Renvoie la liste des utilisateurs ayant demandé l'utilisateur en ami
     * @param c une ConnexionMySQL à la BD
     * @param u l'utilisateur ciblé
     * @return la liste des utilisateurs ayant demandé u en ami
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     */
    public static List<Utilisateur> getDemandesAmis(ConnexionMySQL c, Utilisateur u) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        List<Utilisateur> l = new ArrayList<>();
        ResultSet rs = c.createStatement().executeQuery("select idEnv from REQUETE where activeRQ=1 and idDest="+u.getId());
        while(rs.next()){
            l.add(Utilisateur.instancierParID(c, rs.getInt("idEnv")));
        }
        return l;
    }

    /**
     * Permet de répondre à une demande d'ami
     * @param c une ConnexionMySQL à la BD
     * @param dest l'utilisateur ayant reçu la demande
     * @param env l'utilisateur ayant envoyé la demande
     * @param b true si la demande est acceptée, false sinon
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     */
    public static void repondreDemandeAmi(ConnexionMySQL c, Utilisateur dest, Utilisateur env, boolean b) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        Statement s = c.createStatement();
        if (b){
            s.executeUpdate("update REQUETE set activeRq=0 where idEnv="+env.getId()+" and idDest="+dest.getId());
            s.executeUpdate("insert into ETREAMI values ("+env.getId()+","+dest.getId()+")");
            s.executeUpdate("insert into ETREAMI values ("+dest.getId()+","+env.getId()+")");
        } else {
            s.executeUpdate("update REQUETE set activeRq=0 where idEnv="+env.getId()+" and idDest="+dest.getId());
        }
    }

    /**
     * Permet d'envoyer une demande d'ami
     * @param c une ConnexionMySQL à la BD
     * @param dest l'utilisateur devant recevoir la demande
     * @param env l'utilisateur envoyant la demande
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     */
    public static void envoyerDemandeAmi(ConnexionMySQL c, Utilisateur dest, Utilisateur env) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        ResultSet rs = c.createStatement().executeQuery("select ifnull(max(idRq),0) from REQUETE");
        rs.next();
        int idMax = rs.getInt(1);

        rs = c.createStatement().executeQuery("select exists(select * from INVITATION where idEnv="+env.getId()+" and idDest="+dest.getId()+")");
        rs.next();
        if (!rs.getBoolean(1)) {
            PreparedStatement ps = c.prepareStatement("insert into REQUETE values (?,?,?,now(),?)");
            ps.setInt(1, idMax+1);
            ps.setInt(2, env.getId());
            ps.setInt(3, dest.getId());
            ps.setBoolean(4, true);
            ps.executeUpdate();
        }
    }

    /**
     * Renvoie la liste d'utilisateurs ayant l'utilisateur en ami
     * @param c une ConnexionMySQL à la BD
     * @param u l'utilisateur ciblé
     * @return la liste d'utilisateurs ayant l'utilisateur en ami
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     */
    public static List<Utilisateur> listeAmis(ConnexionMySQL c, Utilisateur u) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        ResultSet rs = c.createStatement().executeQuery("select id2 from ETREAMI where id1="+u.getId());
        List<Utilisateur> l = new ArrayList<>();
        while(rs.next()){
            l.add(Utilisateur.instancierParID(c, rs.getInt("id2")));
        }
        return l;
    }

    /**
     * Renvoie la liste des messages entre les deux utilisateurs
     * @param c une ConnexionMySQL à la BD
     * @param u1 un des deux utilisateurs ciblés
     * @param u2 l'autre utilisateur ciblé
     * @return la liste des messages entre les deux utilisateurs
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     */
    public static List<Message> getConversation(ConnexionMySQL c, Utilisateur u1, Utilisateur u2) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        ResultSet rs = c.createStatement().executeQuery("select idMsg from MESSAGE where idEnv="+u1.getId()+" and idDest="+u2.getId()
                +" or idEnv="+u2.getId()+" and idDest="+u1.getId()+" order by dateMsg");
        List<Message> l = new ArrayList<>();
        while (rs.next()){
            l.add(Message.instancierParID(c, rs.getInt("idMsg")));
        }
        l.sort((Message m1, Message m2)->(m1.getDate().compareTo(m2.getDate())));
//        l.sort(Comparator.comparingInt(Message::getId));
        return l;
    }

    /**
     * Permet d'envoyer un message
     * @param c une ConnexionMySQL à la BD
     * @param env l'utilisateur envoyant le message
     * @param dest l'utilisateur devant recevoir le message
     * @param texte le texte du message
     * @throws SQLException si c est null ou en cas d'erreur de connexion
     */
    public static void envoyerMessage(ConnexionMySQL c, Utilisateur env, Utilisateur dest, String texte) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        ResultSet rs = c.createStatement().executeQuery("select ifnull(max(idMsg),0) from MESSAGE");
        rs.next();
        int idMax = rs.getInt(1);

        PreparedStatement ps = c.prepareStatement("insert into MESSAGE values (?,?,?,now(),?,false)");
        ps.setInt(1, idMax+1);
        ps.setInt(2, env.getId());
        ps.setInt(3, dest.getId());
        ps.setString(4, texte);
        ps.executeUpdate();
    }

    public static List<Invitation> getInvitations(ConnexionMySQL c, Utilisateur cible) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        ResultSet rs = c.createStatement().executeQuery("select idInv from INVITATION where idDest="+cible.getId());
        List<Invitation> l = new ArrayList<>();
        while (rs.next()){
            l.add(Invitation.instancierParID(c, rs.getInt("idInv")));
        }
        return l;
    }

    public static void envoyerInvitation(ConnexionMySQL c, Utilisateur env, Utilisateur dest, Jeu jeu) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        ResultSet rs = c.createStatement().executeQuery("select ifnull(max(idInv),0) from INVITATION");
        rs.next();
        int idMax = rs.getInt(1);
        rs.close();

        PreparedStatement ps = c.prepareStatement("insert into INVITATION values (?,?,?,?,now(),?)");
        ps.setInt(1, idMax+1);
        ps.setInt(2, jeu.getId());
        ps.setInt(3, env.getId());
        ps.setInt(4, dest.getId());
        ps.setBoolean(5, true);
        ps.executeUpdate();
    }

    public static int repondreInvitation(ConnexionMySQL c, Invitation inv, boolean accepte) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        int i = 0;
        if (accepte) {
            ResultSet rs = c.createStatement().executeQuery("select ifnull(max(idPa),0) from PARTIE");
            rs.next();
            int nouvId = i = rs.getInt(1)+1;

            PreparedStatement ps = c.prepareStatement("insert into PARTIE values (?,?,now(),null,false)");
            ps.setInt(1, nouvId);
            ps.setInt(2, inv.getJeu().getId());
            ps.executeUpdate();

            Statement s = c.createStatement();
            s.executeUpdate(String.format("insert into PARTICIPER values (%d,%d,%d)", nouvId, inv.getEnv().getId(), 0));
            s.executeUpdate(String.format("insert into PARTICIPER values (%d,%d,%d)", nouvId, inv.getDest().getId(), 0));
        }
        c.createStatement().executeUpdate("update INVITATION set activeInv=false where idInv="+inv.getId());
        return i;
    }

    public static List<Jeu> getListeJeux(ConnexionMySQL c) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        ResultSet rs = c.createStatement().executeQuery("select idJeu from JEU");
        List<Jeu> l = new ArrayList<>();
        while (rs.next()){
            l.add(Jeu.instancierParID(c, rs.getInt("idJeu")));
        }
        return l;
    }

    public static void setActifJeu(ConnexionMySQL c, Jeu j, boolean activer) throws SQLException {
        if (!c.isConnecte())
            throw new SQLException();
        PreparedStatement ps = c.prepareStatement("update JEU set activeJeu=? where idJeu=?");
        ps.setBoolean(1, activer);
        ps.setInt(2, j.getId());
        ps.executeUpdate();
    }

    public static void setJarJeu(ConnexionMySQL c, Jeu j, byte[] bytes) throws SQLException {
        if (!c.isConnecte())
            throw new SQLException();
        PreparedStatement ps = c.prepareStatement("update JEU set jarJeu=? where idJeu=?");
        Blob blob = c.createBlob();
        blob.setBytes(1, bytes);
        ps.setBlob(1, blob);
        ps.setInt(2, j.getId());
        ps.executeUpdate();
    }

    public static List<Partie> getListeParties(ConnexionMySQL c, Utilisateur cible) throws SQLException {
        if (!c.isConnecte())
            throw new SQLException();
        ResultSet rs = c.createStatement().executeQuery("select idPa from PARTIE natural join PARTICIPER where idUt="+cible.getId());
        List<Partie> l = new ArrayList<>();
        while (rs.next()){
            l.add(Partie.instancierParID(c, rs.getInt("idPa"), cible));
        }
        return l;
    }
}

