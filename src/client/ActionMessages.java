package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Action pour acceder à la liste des conversations
 */
public class ActionMessages implements EventHandler<ActionEvent> {
    private Client app;

    /**
     * Constructeur du controleur de la listes des conversations
     * @param app L'application du client
     */
    public ActionMessages(Client app){
        this.app = app;
    }

    /**
     * Met à jour la fenêtre
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        app.setStage(Messages.setMessages(app), "Duel sur la toile - Messages");
    }
}