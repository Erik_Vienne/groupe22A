package client;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;

/**
 * Onglet des nouveautés
 */
public class Nouveautes {
    /**
     * Créé la page des nouveautes
     * @param app L'application du client
     * @return La scene qui correspond à la page demandée
     */
    public static Scene setNouveautes(Client app){
        BorderPane cont = new BorderPane();
        BorderPane mid = new BorderPane();
        mid.setTop(Template.templateMenus("Nouveautés", app));
        VBox news = new VBox();
        ScrollPane sp = new ScrollPane();



        for(int cpt = 0; cpt<10; cpt++){
            VBox nouveaute = new VBox();

            nouveaute.prefWidthProperty().bind(sp.widthProperty().multiply(0.93));

            Label titre = new Label("Titre");
            Label paragraphe = new Label("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent mattis risus id enim cursus tincidunt. Ut a est ligula. Suspendisse sollicitudin felis in nunc pulvinar, eu sodales ipsum iaculis.");

            paragraphe.setWrapText(true);
//            paragraphe.setMaxWidth(sp.widthProperty());
//            paragraphe.setTextAlignment(TextAlignment.JUSTIFY);

            nouveaute.getChildren().addAll(titre, paragraphe);

            nouveaute.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
            news.getChildren().add(nouveaute);
            news.setVgrow(nouveaute, Priority.ALWAYS);
        }
        news.setPadding(new Insets(10));


        sp.setContent(news);
        sp.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        sp.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        mid.setCenter(sp);
        cont.setCenter(mid);
        cont.setRight(Template.templateJoueurAmis(app, "Classique"));
        return new Scene(cont,app.HAUTEUR,app.LARGEUR);
    }
}
