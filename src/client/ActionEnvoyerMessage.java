package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

import java.sql.SQLException;

/**
 * Action pour envoyer un message
 */
public class ActionEnvoyerMessage  implements EventHandler<ActionEvent> {
    private Client app;
    private Utilisateur ami;
    private String message;


    /**
     * Constructeur du controleur d'envoi de message
     * @param app L'application du client
     * @param ami L'utilisateur avec qui on discute
     * @param message Le message à envoyer
     */
    public ActionEnvoyerMessage(Client app, Utilisateur ami, String message){
        this.app = app;
        this.ami = ami;
        this.message = message;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        try{
            BibliMySQLClient.envoyerMessage(app.mySQL, app.getUtilisateur(), ami, message);
        }
        catch (SQLException e){
            System.out.println("pb");
        }
        app.setStage(Discussion.setDiscussion(app, ami), "Duel sur la toile - Parler avec " + ami.getPseudo());
    }
}
