package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Action pour acceder à la page des parties
 */
public class ActionMesParties implements EventHandler<ActionEvent> {
    private Client app;

    /**
     * Constructeur du controleur des jeux
     * @param app L'application du client
     */
    public ActionMesParties(Client app){
        this.app = app;
    }

    /**
     * Met à jour la fenêtre
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        app.setStage(MesParties.setMesParties(app), "Duel sur la toile - MesParties");
    }
}