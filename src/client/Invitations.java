package client;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Onglet des invitations
 */
public class Invitations {
    public final static int JEUX = 0;
    public final static int AMIS = 1;

    /**
     * Invitations générales
     * @param app L'application du client
     * @param typeInvitation Le type d'invitation, soit jeu soit ami
     * @return La scene qui correspond à la page demandée
     */
    public static Scene setInvitations(Client app, int typeInvitation){

        ActionInvitationsJeux handlerInvitsJeux = new ActionInvitationsJeux(app);
        ActionInvitationsAmis handlerInvitsAmis = new ActionInvitationsAmis(app);

        BorderPane cont = new BorderPane();

        BorderPane invitations = new BorderPane();

        HBox menuInvits = new HBox();
        ToggleGroup group = new ToggleGroup();

        ToggleButton amis = new ToggleButton("Demandes d'amis");
        amis.setToggleGroup(group);
        amis.setOnAction(handlerInvitsAmis);
        amis.setMaxSize(app.LARGEUR, app.LARGEUR);

        ToggleButton jeux = new ToggleButton("Invitations de jeux");
        jeux.setToggleGroup(group);
        jeux.setOnAction(handlerInvitsJeux);
        jeux.setMaxSize(app.LARGEUR, app.LARGEUR);

        menuInvits.getChildren().addAll(amis, jeux);
        HBox.setHgrow(amis, Priority.ALWAYS);
        HBox.setHgrow(jeux, Priority.ALWAYS);



        invitations.setTop(menuInvits);
        switch (typeInvitation){
            case Invitations.AMIS:
                invitations.setCenter(Invitations.setDemandesAmis(app));
                amis.setSelected(true);
                break;
            case Invitations.JEUX:
                invitations.setCenter(Invitations.setDemandesJeux(app));
                jeux.setSelected(true);
                break;
        }
        BorderPane mid = new BorderPane();
        mid.setTop(Template.templateMenus("Invitations", app));
        mid.setCenter(invitations);
        cont.setCenter(mid);
        cont.setRight(Template.templateJoueurAmis(app, "Classique"));
        return new Scene(cont,app.HAUTEUR,app.LARGEUR);

    }

    /**
     * Pour affichet les demandes d'amis
     * @param app L'application du client
     * @return Le conteneur pour les demandes d'amis
     */
    public static BorderPane setDemandesAmis(Client app){

        BorderPane res = new BorderPane();
        VBox demandes = new VBox();
        List<Utilisateur> demandesRecues = new ArrayList<>();
        try{
            demandesRecues = BibliMySQLClient.getDemandesAmis(app.mySQL, app.getUtilisateur());

        }
        catch (SQLException e){
            System.out.println("PB");
        }

        System.out.println(demandesRecues);

        if(demandesRecues.size()>0) {
            for (Utilisateur user : demandesRecues) {
                BorderPane lademande = new BorderPane();
                Label nomDemande = new Label("Demande de : " + user.getPseudo());

                Button accepter = new Button("Accepter");
                accepter.setOnAction(new ActionAccepterRefuserAmi(app, accepter.getText(), user.getPseudo()));

                Button refuser = new Button("Refuser");
                refuser.setOnAction(new ActionAccepterRefuserAmi(app, refuser.getText(), user.getPseudo()));

                BorderPane.setMargin(accepter, new Insets(0, 10, 0, 10));
                BorderPane.setMargin(refuser, new Insets(0, 10, 0, 10));

                lademande.setLeft(nomDemande);
                lademande.setCenter(accepter);
                lademande.setRight(refuser);

                demandes.getChildren().add(lademande);
            }
            res.setCenter(new ScrollPane(demandes));

        }

        else{
            Label pasDAmis = new Label("Vous n'avez pas de demande d'amis :(");
            pasDAmis.setAlignment(Pos.CENTER);
            res.setCenter(pasDAmis);
        }



        ActionAjouterAmi ajouterUnAmi = new ActionAjouterAmi(app);
        Button ajouter = new Button("+");
        ajouter.setShape(new Circle(2));
        ajouter.setOnAction(ajouterUnAmi);
        BorderPane coinBasDroite = new BorderPane();
        coinBasDroite.setRight(ajouter);
        res.setBottom(coinBasDroite);
        return res;

    }

    /**
     * Pour affichet les demandes de jeux
     * @param app L'application du client
     * @return Le conteneur pour les demandes de jeux
     */
    public static BorderPane setDemandesJeux(Client app){
        BorderPane res = new BorderPane();
        VBox demandes = new VBox();
        List<Invitation> invitationsRecues = new ArrayList<>();
        try{
            invitationsRecues = BibliMySQLClient.getInvitations(app.mySQL, app.getUtilisateur());

        }
        catch (SQLException e){
            e.printStackTrace();
        }

        System.out.println(invitationsRecues);

        if(invitationsRecues.size()>0) {
            for (Invitation invit : invitationsRecues) {
                if(invit.isActif()){
                    BorderPane lademande = new BorderPane();
                    Label nomDemande = new Label("Demande de " + invit.getEnv().getPseudo() + " pour jouer à : " + invit.getJeu().getNom());

                    Button accepter = new Button("Accepter");
                    accepter.setOnAction(new ActionAccepterRefuserInvitation(app, accepter.getText(), invit));

                    Button refuser = new Button("Refuser");
                    refuser.setOnAction(new ActionAccepterRefuserInvitation(app, refuser.getText(), invit));

                    BorderPane.setMargin(accepter, new Insets(0, 10, 0, 10));
                    BorderPane.setMargin(refuser, new Insets(0, 10, 0, 10));

                    lademande.setLeft(nomDemande);
                    lademande.setCenter(accepter);
                    lademande.setRight(refuser);
                    demandes.getChildren().add(lademande);
                }
            }
            res.setCenter(new ScrollPane(demandes));

        }

        else{
            Label pasDAmis = new Label("Vous n'avez pas d'invitation de jeu :(");
            pasDAmis.setAlignment(Pos.CENTER);
            res.setCenter(pasDAmis);
        }



        ActionInviterAJouer inviterAmi = new ActionInviterAJouer(app);
        Button ajouter = new Button("+");
        ajouter.setShape(new Circle(2));
        ajouter.setOnAction(inviterAmi);
        BorderPane coinBasDroite = new BorderPane();
        coinBasDroite.setRight(ajouter);
        res.setBottom(coinBasDroite);
        return res;
    }
}
