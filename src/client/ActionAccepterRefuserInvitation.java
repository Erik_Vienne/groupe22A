package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.io.File;
import java.io.InputStream;
import java.sql.SQLException;

/**
 * Action des boutons de réponse à des invitations de jeu.
 * Permet d'accepter ou de refuser une invitation de jeu.
 */
public class ActionAccepterRefuserInvitation   implements EventHandler<ActionEvent> {
    private Client app;
    private String reponse;
    private Invitation invit;

    /**
     * Constructeur du contrôleur de réponse d'invitation de jeu
     * @param app L'application du client
     * @param reponse Le texte du bouton appuyé
     * @param invit L'invitation
     */
    public ActionAccepterRefuserInvitation(Client app, String reponse, Invitation invit){
        this.app = app;
        this.reponse = reponse;
        this.invit = invit;

    }

    @Override
    public void handle(ActionEvent actionEvent) {
        Partie partie = null;
        try{
            partie = Partie.instancierParID(app.mySQL, BibliMySQLClient.repondreInvitation(app.mySQL, invit, reponse.equals("Accepter")), app.getUtilisateur());
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        String jeuSelect = partie.getJeu().getNom();


        if(reponse.equals("Accepter")){
            try {
//                Jeu j = Jeu.instancierParNom(app.mySQL, jeuSelect);
//                j.telechargerJeu();
                if (jeuSelect.equals("Morpion"))
                    new morpion.MorpionAccueil().demarrer(partie.getId(), app.getUtilisateur().getId());

                if (jeuSelect.equals("Mastermind")||jeuSelect.equals("Puissance 4")) {
                    ProcessBuilder pb = new ProcessBuilder("/usr/bin/java", "-jar", "jars/" + jeuSelect + ".jar");
                    pb.directory(new File("."));
                    Process p = pb.start();
                    p.waitFor();
                }
//                InputStream in = p.getInputStream();
//                byte b[]=new byte[in.available()];
//                in.read(b,0,b.length);
//                System.out.println(new String(b));
//
//                InputStream err = p.getErrorStream();
//                byte c[]=new byte[err.available()];
//                err.read(c,0,c.length);
//                System.err.println(new String(c));
                app.setStage(Invitations.setInvitations(app, Invitations.JEUX), "Duel sur la toile - Invitations");
            } catch (Exception e){e.printStackTrace();}
        }
        else{
            ActionInvitationsJeux handler = new ActionInvitationsJeux(app);
            handler.handle(new ActionEvent());
        }


    }
}

