package client;


import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.ByteArrayInputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Onglet de ses parties
 */
public class MesParties {

    /**
     * Créé la page de ses parties
     * @param app L'application du client
     * @return La scene qui correspond à la page demandée
     */
    public static Scene setMesParties(Client app) {
        BorderPane cont = new BorderPane();
        BorderPane mid = new BorderPane();

        List<Partie> mesParties = new ArrayList<>();
        VBox listeParties = new VBox();
        ScrollPane sp = new ScrollPane();

        try{
            mesParties = BibliMySQLClient.getListeParties(app.mySQL, app.getUtilisateur());
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        for (Partie partie : mesParties) {
            HBox game = new HBox();
            Label nomPartie = new Label("Partie de " + partie.getJeu().getNom() + " avec : "  + partie.getAdversaire().getPseudo());

            Button jouer = new Button("Jouer");
            jouer.setOnAction(new ActionClickJeu(app, partie));


            BorderPane.setMargin(jouer, new Insets(0, 10, 0, 10));

            game.getChildren().addAll(nomPartie, jouer);
            listeParties.getChildren().add(game);
        }


        sp.setContent(listeParties);
        mid.setTop(Template.templateMenus("Mes Parties", app));
        mid.setCenter(sp);
        cont.setCenter(mid);
        cont.setRight(Template.templateJoueurAmis(app, "Classique"));
        return new Scene(cont, app.HAUTEUR,app.LARGEUR);
    }
}