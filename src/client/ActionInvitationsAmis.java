package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Action pour acceder à la page des invitations d'ami
 */
public class ActionInvitationsAmis implements EventHandler<ActionEvent> {
    private Client app;

    /**
     * Constructeur du controleur des demandes d'ami
     * @param app L'application du client
     */
    public ActionInvitationsAmis(Client app){
        this.app = app;
    }

    /**
     * Met à jour la fenêtre
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        app.setStage(Invitations.setInvitations(app, Invitations.AMIS), "Duel sur la toile - Invitations");
    }
}
