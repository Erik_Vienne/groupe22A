package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.sql.SQLException;

/**
 * Action des boutons de réponse à des demandes d'amis.
 * Permet d'accepter ou de refuser une demande d'ami.
 */
public class ActionAccepterRefuserAmi  implements EventHandler<ActionEvent> {
    private Client app;
    private String reponse;
    private Utilisateur demandeur;

    /**
     * Constructeur du contrôleur de réponse d'amis
     * @param app L'application du client
     * @param reponse Le texte du bouton appuyé
     * @param nomDemandeur Le pseudo de la personne qui demande en ami
     */
    public ActionAccepterRefuserAmi(Client app, String reponse, String nomDemandeur){
        this.app = app;
        this.reponse = reponse;
        try{
            this.demandeur = Utilisateur.instancierParPseudo(app.mySQL, nomDemandeur);
        }
        catch(SQLException e){
            System.out.println(e);
        }

    }

    @Override
    public void handle(ActionEvent actionEvent) {
        try{
            BibliMySQLClient.repondreDemandeAmi(app.mySQL, app.getUtilisateur(), demandeur, reponse.equals("Accepter"));
        }
        catch (SQLException e){
            System.out.println(e);
        }
        ActionInvitationsAmis handler = new ActionInvitationsAmis(app);
        handler.handle(new ActionEvent());
    }
}
