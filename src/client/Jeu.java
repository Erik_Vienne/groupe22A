package client;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Jeu {
    private int id;
    private boolean actif, solo;
    private String nom, regles;
    private byte[] jar;

    private Jeu(int id, String nom, String regles, boolean actif, boolean solo, byte[] jar){
        this.id=id;
        this.nom=nom;
        this.regles=regles;
        this.actif=actif;
        this.solo=solo;
        this.jar =jar;
    }

    public static Jeu instancierParID(ConnexionMySQL c, int id) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        ResultSet rs = c.createStatement().executeQuery("select * from JEU where idJeu="+id);
        rs.next();
        Blob b = rs.getBlob("jarJeu");
        System.out.println((int)b.length());
        return new Jeu(id, rs.getString("nomJeu"), rs.getString("regleJeu"), rs.getBoolean("actifJeu"), rs.getBoolean("soloJeu"), b.getBytes(1, (int)b.length()));
    }

    public static Jeu instancierParNom(ConnexionMySQL c, String nom) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        ResultSet rs = c.createStatement().executeQuery("select * from JEU where nomJeu='"+nom+"'");
        rs.next();
        Blob b = rs.getBlob("jarJeu");
        System.out.println((int)b.length());
        return new Jeu(rs.getInt("idJeu"), nom, rs.getString("regleJeu"), rs.getBoolean("actifJeu"), rs.getBoolean("soloJeu"), b.getBytes(1, (int)b.length()));
    }

    public int getId() {
        return id;
    }

    public boolean isActif() {
        return actif;
    }

    public boolean isSolo() {
        return solo;
    }

    public String getNom() {
        return nom;
    }

    public String getRegles() {
        return regles;
    }

    public byte[] getJar() {
        return jar;
    }

    public void telechargerJeu(){
        File f = new File("jars/"+this.nom+".jar");
        try {
            FileOutputStream writer = new FileOutputStream(f);
            writer.write(this.jar);
            writer.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}

