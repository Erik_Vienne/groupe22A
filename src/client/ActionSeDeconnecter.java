package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;

/**
 * Action du bouton pour se déconnecter
 */
public class ActionSeDeconnecter implements EventHandler<ActionEvent> {
    private Client app;

    /**
     * Constructeur du controleur de deconnexion
     * @param app L'application du client
     */
    public ActionSeDeconnecter(Client app){
        this.app = app;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        app.setUtilisateur(null);
        app.setStage(app.laScene(), "Duel sur la toile - Connexion");
    }
}
