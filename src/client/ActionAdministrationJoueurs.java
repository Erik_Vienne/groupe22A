package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Action du bouton pour acceder à l'administration des joueurs
 */
public class ActionAdministrationJoueurs implements EventHandler<ActionEvent> {
    private Client app;

    /**
     * Constructeur du controleur de l'administration des joueurs
     * @param app L'application du client
     */
    public ActionAdministrationJoueurs(Client app){
        this.app = app;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        app.setStage(AdministrationJoueurs.setAdministrationJoueurs(app), "Duel sur la toile - Administration");
    }
}
