package client;

import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;

import java.io.File;
import java.nio.file.Files;
import java.util.Optional;

/**
 * Action pour administrer les Jeux
 */
public class ActionClickAdministrationJeux implements EventHandler<MouseEvent> {
    private Client app;
    private FileChooser fc;

    /**
     * Constructeur du controleur pour acceder à l'onglet administration des jeux
     * @param app L'application du client
     */
    public ActionClickAdministrationJeux(Client app){
        this.app=app;
        this.fc = new FileChooser();
        this.fc.setInitialDirectory(new File("."));
    }

    /**
     * Fait apparaitre une pop-up lors d'un double click sur le nom d'un jeu
     * Les fonctions d'administration sont : Activer un jeu, le désactiver, le modifier ou annuler
     */
    @Override
    public void handle(MouseEvent click) {
        String jeuSelect = app.list.getSelectionModel().getSelectedItem().toString();

        if (click.getClickCount() == 2) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("nom du jeu");
            alert.setHeaderText("Quelle modification voulez-vous faire à ce jeu ?");
//            alert.setContentText("Choose your option.");

            ButtonType buttonTypeActiver = new ButtonType("Activer");
            ButtonType buttonTypeDesactiver = new ButtonType("Desactiver");
            ButtonType buttonTypeModifier = new ButtonType("Modifier");

            ButtonType buttonTypeAnnuler = new ButtonType("Annuler", ButtonBar.ButtonData.CANCEL_CLOSE);

            alert.getButtonTypes().setAll(buttonTypeActiver, buttonTypeDesactiver,buttonTypeModifier, buttonTypeAnnuler);

            Optional<ButtonType> result = alert.showAndWait();
            try {
                if (result.get() == buttonTypeActiver) {
                    BibliMySQLClient.setActifJeu(app.mySQL, Jeu.instancierParNom(app.mySQL, jeuSelect), true);
                } else if (result.get() == buttonTypeDesactiver) {
                    BibliMySQLClient.setActifJeu(app.mySQL, Jeu.instancierParNom(app.mySQL, jeuSelect), false);
                } else if (result.get() == buttonTypeModifier) {
                    File f = this.fc.showOpenDialog(null);
                    System.out.println(Files.probeContentType(f.toPath()));
                    if (Files.probeContentType(f.toPath()).equals("application/x-java-archive"))
                        BibliMySQLClient.setJarJeu(app.mySQL, Jeu.instancierParNom(app.mySQL, jeuSelect), Files.readAllBytes(f.toPath()));
                }
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}

