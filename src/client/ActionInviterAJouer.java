package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextInputDialog;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Action pour inviter un ami à jouer
 */
public class ActionInviterAJouer implements EventHandler<ActionEvent> {
    Client app;
    Utilisateur AmiAvecQuiJouer;

    /**
     * Constructeur du controleur pour ajouter un ami
     * @param app L'application du client
     */
    public ActionInviterAJouer (Client app){
        this.app = app;
    }

    /**
     * Fait apparaitre une pop-up pour écrire le nom de la personne qui doit être ajoutée et envoie la demande
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        List<Utilisateur> listeAmis = new ArrayList<>();
        try {
            listeAmis = BibliMySQLClient.listeAmis(app.mySQL, app.getUtilisateur());
        } catch (SQLException e) {
            System.out.println("pb listeamis");
        }
        if (listeAmis.size() > 0) {
            List<String> choix = new ArrayList<>();
            try {
                for (Jeu jeu : BibliMySQLClient.getListeJeux(app.mySQL)) {
                    choix.add(jeu.getNom());
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            String nomDuJeu = new String();
            ChoiceDialog<String> dialog = new ChoiceDialog<>(choix.get(0), choix);
            dialog.setTitle("Inviter un ami à jouer");
            dialog.setHeaderText("A quel jeu voulez vous jouer ?");
            dialog.setContentText("Nom du jeu :");

// Traditional way to get the response value.
            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()) {
                nomDuJeu = result.get();
            }

            List<String> choix2 = new ArrayList<>();
            try {
                for (Utilisateur user : BibliMySQLClient.listeAmis(app.mySQL, app.getUtilisateur())) {
                    choix2.add(user.getPseudo());
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            ChoiceDialog<String> dialog2 = new ChoiceDialog<>(choix2.get(0), choix2);
            dialog2.setTitle("Inviter un ami à jouer");
            dialog2.setHeaderText("Avec qui voulez vous jouer ?");
            dialog2.setContentText("Nom de l'ami :");

            Optional<String> result2 = dialog2.showAndWait();
            if (result2.isPresent()) {
                try {
                    Utilisateur dest = Utilisateur.instancierParPseudo(app.mySQL, result2.get());
                    BibliMySQLClient.envoyerInvitation(app.mySQL, app.getUtilisateur(), dest, Jeu.instancierParNom(app.mySQL, nomDuJeu));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Invitation à jouer");
            alert.setHeaderText("Vous ne pouvez pas envoyer d'invitation");
            alert.setContentText("Vous n'avez pas d'amis :/");

            alert.showAndWait();
        }

    }
}
