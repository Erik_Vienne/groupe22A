package client;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

import java.sql.SQLException;
import java.util.List;

/**
 * Administration des jeux
 */
public class AdministrationJeux {


    /**
     * Créé la page d'administration des jeux
     * @param app L'application du client
     * @return La scene qui correspond à la page demandée
     */
    public static Scene setAdministrationJeux(Client app) {
        app.entries = FXCollections.observableArrayList();
        app.entries.clear();
        app.list = new ListView();
        BorderPane cont = new BorderPane();
        BorderPane mid = new BorderPane();
        mid.setTop(Template.templateMenusAdmin("MesParties", app));

        TextField recherche = new TextField();

        recherche.setPromptText("Rechercher Jeu");
        recherche.textProperty().addListener((ChangeListener) (observable, oldVal, newVal) -> app.search((String) oldVal, (String) newVal));




        try{
            List<Jeu> listeJeux = BibliMySQLClient.getListeJeux(app.mySQL);
            System.out.println("liste des jeux");
            System.out.println(listeJeux.toString());
            for(Jeu jeu : listeJeux){
                String game = jeu.getNom();
                System.out.println(game);
                app.entries.add(game);
            }
        }

        catch (SQLException e){
            System.out.println("Probleme");
        }


        app.list.setItems(app.entries);
        app.list.setOnMouseClicked(new ActionClickAdministrationJeux(app));



        VBox administrationJeux = new VBox();
        administrationJeux.setPadding(new Insets(10, 10, 10, 10));
        administrationJeux.setSpacing(2);
        administrationJeux.getChildren().addAll(recherche, app.list);




        mid.setCenter(administrationJeux);
        cont.setCenter(mid);
        cont.setRight(Template.templateJoueurAmis(app, "Administration"));
        return new Scene(cont, app.HAUTEUR, app.LARGEUR);
    }
}