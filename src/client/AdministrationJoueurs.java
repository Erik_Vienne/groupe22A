package client;

import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

import java.sql.SQLException;
import java.util.List;

/**
 * Administration des joueurs
 */
public class AdministrationJoueurs {


    /**
     * Créé la page d'administration des joueurs
     * @param app L'application du client
     * @return La scene qui correspond à la page demandée
     */
    public static Scene setAdministrationJoueurs(Client app) {
        app.entries = FXCollections.observableArrayList();
        app.entries.clear();
        app.list = new ListView();
        BorderPane cont = new BorderPane();
        BorderPane mid = new BorderPane();
        mid.setTop(Template.templateMenusAdmin("Joueurs", app));

        TextField recherche = new TextField();

        recherche.setPromptText("Rechercher Joueur");
        recherche.textProperty().addListener((ChangeListener) (observable, oldVal, newVal) -> app.search((String) oldVal, (String) newVal));




        try{
            List<Utilisateur> utilisateurs = BibliMySQLClient.listeTousJoueurs(BibliMySQLClient.getConnexion());
            for(Utilisateur user : utilisateurs){
                String pseudo = user.getPseudo();
                app.entries.add(pseudo);

            }
        }

        catch (SQLException e){
            System.out.println("Probleme");
        }


        app.list.setItems(app.entries);
        app.itemSelectione = "";
        app.list.setOnMouseClicked(new ActionClickAdministrationJoueur(app.itemSelectione, app));


        VBox administrationJoueurs = new VBox();
        administrationJoueurs.setPadding(new Insets(10, 10, 10, 10));
        administrationJoueurs.setSpacing(2);
        administrationJoueurs.getChildren().addAll(recherche, app.list);




        mid.setCenter(administrationJoueurs);
        cont.setCenter(mid);
        cont.setRight(Template.templateJoueurAmis(app, "Administration"));
        return new Scene(cont, app.HAUTEUR, app.LARGEUR);
    }



}
