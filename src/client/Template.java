package client;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.io.ByteArrayInputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Modèle des menus
 */
public class Template {
    /**
     * Modele pour le menu de navigation
     * @param nomPage Le nom l'onglet actif
     * @param app L'application du client
     * @return Conteneur contenant le menu de navigation
     */
    public static HBox templateMenus(String nomPage, Client app) {
        ActionNouveautes handlerNews = new ActionNouveautes(app);
        ActionMesParties handlerMesParties = new ActionMesParties(app);
        ActionInvitationsAmis handlerInvitations = new ActionInvitationsAmis(app);
        ActionMessages handlerMessages = new ActionMessages(app);
        HBox menu = new HBox();
        ToggleGroup group = new ToggleGroup();

        ToggleButton news = new ToggleButton("Nouveautés");
        news.setToggleGroup(group);
        news.setOnAction(handlerNews);
        news.setMaxSize(app.LARGEUR, app.LARGEUR);


        ToggleButton mesParties = new ToggleButton("Mes Parties");
        mesParties.setToggleGroup(group);
        mesParties.setOnAction(handlerMesParties);
        mesParties.setMaxSize(app.LARGEUR, app.LARGEUR);


        ToggleButton invits = new ToggleButton("Invitations");
        invits.setToggleGroup(group);
        invits.setOnAction(handlerInvitations);
        invits.setMaxSize(app.LARGEUR, app.LARGEUR);


        ToggleButton messages = new ToggleButton("Messages");
        messages.setToggleGroup(group);
        messages.setOnAction(handlerMessages);
        messages.setMaxSize(app.LARGEUR, app.LARGEUR);


        switch (nomPage) {
            case "Nouveautés":
                news.setSelected(true);
                break;
            case "Mes Parties":
                mesParties.setSelected(true);
                break;
            case "Invitations":
                invits.setSelected(true);
                break;
            case "Messages":
                messages.setSelected(true);
                break;
        }


//        menu.add(news, 0, 0);
//        menu.add(jeux, 1,0);
//        menu.add(invits, 2, 0);
//        menu.add(messages, 3, 0);

        HBox.setHgrow(news, Priority.ALWAYS);
        HBox.setHgrow(mesParties, Priority.ALWAYS);
        HBox.setHgrow(invits, Priority.ALWAYS);
        HBox.setHgrow(messages, Priority.ALWAYS);

        menu.getChildren().addAll(news, mesParties, invits, messages);
        menu.setAlignment(Pos.CENTER);
        menu.setPadding(new Insets(10));
        return menu;
    }

    /**
     * Modele pour l'utilisateur et les amis
     * @param app L'application du client
     * @param partieVueActuelle patie pour savoir si on est dans classique ou administration
     * @return Conteneur contenant le joueur et ses amis
     */
    public static VBox templateJoueurAmis(Client app, String partieVueActuelle) {
        VBox joueurAmis = new VBox(2);
        Label pseudo;
        HBox joueur = new HBox(2);
        ImageView img = new ImageView();
        img.setImage(new Image(new ByteArrayInputStream(app.getUtilisateur().getAvatar())));
        img.setFitWidth(60);
        img.setFitHeight(60);
        img.setOnMouseClicked(new ActionAvatar(app, img));
        pseudo = new Label(app.getUtilisateur().getPseudo());
        joueur.getChildren().addAll(img, pseudo);
        VBox liens = new VBox();
        if(app.getUtilisateur().estAdmin()){
            switch (partieVueActuelle){
                case "Administration":
                    ActionNouveautes handlerNouveautes = new ActionNouveautes(app);
                    Hyperlink lienRetour = new Hyperlink();
                    lienRetour.setText("Retour");
                    lienRetour.setOnAction(handlerNouveautes);
                    liens.getChildren().add(lienRetour);
                    break;
                case "Classique":
                    ActionAdministrationJoueurs handlerAdministrationJoueurs = new ActionAdministrationJoueurs(app);
                    Hyperlink lienAdministration = new Hyperlink();
                    lienAdministration.setText("Administration");
                    lienAdministration.setOnAction(handlerAdministrationJoueurs);
                    liens.getChildren().add(lienAdministration);
                    break;
            }
        }

        ActionSeDeconnecter handlerDeconnexion = new ActionSeDeconnecter(app);
        Hyperlink lienDeconnexion = new Hyperlink();
        lienDeconnexion.setText("Se déconnecter");
        lienDeconnexion.setOnAction(handlerDeconnexion);
        liens.getChildren().add(lienDeconnexion);
        joueur.getChildren().add(liens);

        joueur.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

        ScrollPane scrollPane = new ScrollPane();
        VBox listeAmis = new VBox();
        List<Utilisateur> mesAmis = new ArrayList<>();
        try{
            mesAmis = BibliMySQLClient.listeAmis(app.mySQL, app.getUtilisateur());
        }
        catch(SQLException e){
            System.out.println("pb");
        }

        for (Utilisateur user : mesAmis) {
            GridPane ami = new GridPane();
            img = new ImageView();
            img.setImage(new Image(new ByteArrayInputStream(user.getAvatar())));
            img.setFitWidth(60);
            img.setFitHeight(60);
            pseudo = new Label(user.getPseudo());

            ami.setPadding(new Insets(10));
            ami.add(img, 0, 0);
            ami.add(pseudo, 1, 0);
            ami.setOnMouseClicked(new ActionMessageToDiscussion(app, user));

            ami.setOnMouseEntered(new EventHandler<MouseEvent>() {
                public void handle(MouseEvent me) {
                    ami.setStyle("-fx-background-color: #CCCCCC;");
                }
            });

            ami.setOnMouseExited(new EventHandler<MouseEvent>() {
                public void handle(MouseEvent me) {
                    ami.setStyle("");
                }
            });
            
            listeAmis.getChildren().add(ami);
        }
        scrollPane.setContent(listeAmis);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);

        joueurAmis.getChildren().addAll(joueur, scrollPane);

        return joueurAmis;
    }

    /**
     * Modele pour le menu de navigation d'administration
     * @param nomPage Le nom l'onglet actif
     * @param app L'application du client
     * @return Conteneur contenant le menu de navigation d'administration
     */
    public static HBox templateMenusAdmin(String nomPage, Client app) {

        ActionAdministrationJoueurs handlerJoueurs = new ActionAdministrationJoueurs(app);
        ActionAdministrationJeux handlerJeux = new ActionAdministrationJeux(app);
        ActionAdministrationStats handlerStats = new ActionAdministrationStats(app);

        HBox menu = new HBox();
        ToggleGroup group = new ToggleGroup();

        ToggleButton joueurs = new ToggleButton("Joueurs");
        joueurs.setToggleGroup(group);
        joueurs.setOnAction(handlerJoueurs);
        joueurs.setMaxSize(app.LARGEUR, app.LARGEUR);


        ToggleButton jeux = new ToggleButton("Jeux");
        jeux.setToggleGroup(group);
        jeux.setOnAction(handlerJeux);
        jeux.setMaxSize(app.LARGEUR, app.LARGEUR);


        ToggleButton stats = new ToggleButton("Statistiques");
        stats.setToggleGroup(group);
        stats.setOnAction(handlerStats);
        stats.setMaxSize(app.LARGEUR, app.LARGEUR);


        switch (nomPage) {
            case "Joueurs":
                joueurs.setSelected(true);
                break;
            case "Jeux":
                jeux.setSelected(true);
                break;
            case "Statistiques":
                stats.setSelected(true);
                break;
        }
        HBox.setHgrow(joueurs, Priority.ALWAYS);
        HBox.setHgrow(jeux, Priority.ALWAYS);
        HBox.setHgrow(stats, Priority.ALWAYS);

        menu.getChildren().addAll(joueurs, jeux, stats);
        menu.setAlignment(Pos.CENTER);
        menu.setPadding(new Insets(10));
        return menu;

    }
}
