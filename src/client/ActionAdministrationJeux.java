package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Action du bouton pour acceder à l'administration des jeux.
 */
public class ActionAdministrationJeux implements EventHandler<ActionEvent> {
    private Client app;

    /**
     * Constructeur du controleur de l'administration des jeux
     * @param app L'application du client
     */
    public ActionAdministrationJeux(Client app){
        this.app = app;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        app.setStage(AdministrationJeux.setAdministrationJeux(app), "Duel sur la toile - Administration");
    }
}