package client;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * Client de l'application
 */
public class Client extends Application{

    public final static int LARGEUR = 800;
    public final static int HAUTEUR = 700;

    public Stage stage;

    public ObservableList<String> entries;
    public ListView list;
    public String itemSelectione;

    public ConnexionMySQL mySQL;

    private Utilisateur utilisateur;

    private Label loginMessage;
    private TextField txtPseudo, txtMdp;

    private Label createMessage;
    private TextField txtCreateMail, txtCreatePseudo, txtCreateMdp, txtCreateConfirmMdp;

    /**
     * Titre de l'application
     * @return Une conteneur contenant le titre
     */
    private FlowPane titre(){
        FlowPane res = new FlowPane();
        // A implémenter
        Label titre = new Label("Duel sur la toile");
        titre.setFont(new Font(30));
        res.getChildren().add(titre);
        res.setAlignment(Pos.TOP_CENTER);
        return res;
    }

    /**
     * Zone de login
     * @return Une conteneur contenant tous les éléments nécéssaires à la connexion
     */
    private GridPane login(){
        //loginPane
        GridPane loginPane = new GridPane();
        loginPane.setPadding(new Insets(20,20,20,20));
        loginPane.setHgap(5);
        loginPane.setVgap(5);

        //Futurs éléments de loginPane
        Label lblPseudo = new Label("pseudo");
        this.txtPseudo = new TextField();
        Label lblMdp = new Label("mot de passe");
        this.txtMdp = new PasswordField();

        Button btnLogin = new Button("Se connecter");
        this.loginMessage = new Label();

        //Action de btnLogin
        ActionLogin handler = new ActionLogin(this);
        btnLogin.setOnAction(handler);

        this.txtMdp.setOnKeyPressed((KeyEvent ke) ->{
            if(ke.getCode().equals(KeyCode.ENTER)){
                handler.handle(new ActionEvent());
            }
        });

        //Ajout des éléments dans loginPane
        loginPane.add(lblPseudo, 0, 0);
        loginPane.add(txtPseudo, 1, 0);
        loginPane.add(lblMdp, 0, 1);
        loginPane.add(txtMdp, 1, 1);
        loginPane.add(btnLogin, 2, 1);
        loginPane.add(this.loginMessage, 1, 2);

        loginPane.setAlignment(Pos.CENTER_RIGHT);
        return loginPane;
    }


    public void setLoginMessage(String s){
        this.loginMessage.setText(s);
    }

    public String getLoginPseudo(){
        return this.txtPseudo.getText();
    }

    public String getLoginMdp(){
        return this.txtMdp.getText();
    }

    public Utilisateur getUtilisateur(){
        return this.utilisateur;
    }

    public void setUtilisateur(Utilisateur u) {
        this.utilisateur = u;
    }

    /**
     * Zone de création de compte
     * @return Une conteneur contenant tous les éléments nécéssaires à la création de compte
     */
    private GridPane creationCompte(){
        //createPane
        GridPane createPane = new GridPane();
        createPane.setPadding(new Insets(20,20,20,20));
        createPane.setHgap(5);
        createPane.setVgap(5);

        //Futurs éléments de createPane
        Label lblCreateMail = new Label("Adresse mail");
        this.txtCreateMail = new TextField();
        lblCreateMail.setAlignment(Pos.CENTER_LEFT);


        Label lblCreatePseudo = new Label("Pseudo");
        this.txtCreatePseudo = new TextField();
        lblCreatePseudo.setAlignment(Pos.CENTER_LEFT);


        Label lblCreateMdp = new Label("Mot de passe");
        this.txtCreateMdp = new PasswordField();
        lblCreateMdp.setAlignment(Pos.CENTER_LEFT);


        Label lblCreateConfirmMdp = new Label("Confirmer\nmot de passe");
        this.txtCreateConfirmMdp = new PasswordField();
        lblCreateConfirmMdp.setAlignment(Pos.CENTER_LEFT);
        Button btnCreate = new Button("Creer le compte");
        this.createMessage = new Label();

        //Action de btnCreer
        ActionCreer handler = new ActionCreer(this);
        btnCreate.setOnAction(handler);
        this.txtCreateConfirmMdp.setOnKeyPressed((KeyEvent ke) ->{
            if(ke.getCode().equals(KeyCode.ENTER)){
                handler.handle(new ActionEvent());
            }
        });

        //Ajout des éléments dans createPane
        createPane.add(lblCreateMail, 0, 0);
        createPane.add(this.txtCreateMail, 1, 0);
        createPane.add(lblCreatePseudo, 0, 1);
        createPane.add(this.txtCreatePseudo, 1, 1);
        createPane.add(lblCreateMdp, 0, 2);
        createPane.add(this.txtCreateMdp, 1, 2);
        createPane.add(lblCreateConfirmMdp, 0, 3);
        createPane.add(this.txtCreateConfirmMdp, 1, 3);
        createPane.add(btnCreate, 2, 4);
        createPane.add(this.createMessage, 1, 4);

        createPane.setAlignment(Pos.CENTER_LEFT);
        return createPane;
    }

    public void setCreateMessage(String s){
        this.createMessage.setText(s);
    }

    public String getCreateMail(){
        return this.txtCreateMail.getText();
    }

    public String getCreatePseudo(){
        return this.txtCreatePseudo.getText();
    }

    public String getCreateMdp(){
        return this.txtCreateMdp.getText();
    }

    public String getCreateConfirmMdp(){
        return this.txtCreateConfirmMdp.getText();
    }

    /**
     * Scene de l'écran total
     * @return La scene contenant tous les éléments de l'écran initial
     */
    public Scene laScene(){
        BorderPane cont = new BorderPane();
        cont.setTop(this.titre());
        cont.setRight(this.creationCompte());
        cont.setLeft(this.login());
        Separator sep = new Separator();
        sep.setOrientation(Orientation.VERTICAL);
        cont.setCenter(sep);
        return new Scene(cont,700,800);
    }

    /**
     * Permet de changer l'écran
     * @param scene La scene qui remplacera la précedente
     * @param titre Le titre de la fenêtre
     */
    public void setStage(Scene scene, String titre){

        this.stage.setTitle(titre);
        this.stage.setScene(scene);
    }

    @Override
    public void stop() throws Exception {
        this.mySQL.close();
    }

    /**
     * Permet de la recherche dynamique
     * @param oldVal L'ancienne valeur
     * @param newVal La nouvelle valeur
     */
    public void search(String oldVal, String newVal) {
        if (oldVal != null && (newVal.length() < oldVal.length())) {
            this.list.setItems(this.entries);
        }

        String value = newVal.toUpperCase();
        ObservableList<String> subentries = FXCollections.observableArrayList();
        for (Object entry : this.list.getItems()) {
            boolean match = true;
            String entryText = (String) entry;
            if (!entryText.toUpperCase().contains(value)) {
                match = false;
            }
            if (match) {
                subentries.add(entryText);
            }
        }
        this.list.setItems(subentries);
    }

    @Override
    /**
     * Lance l'application
     */
    public void start(Stage stage) {
        this.stage = stage;
//        this.stage.setResizable(false);
        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();

        this.stage.setTitle("Duel sur la toile - Connexion");
        this.stage.setScene(this.laScene());
        this.txtPseudo.requestFocus();

        this.mySQL = BibliMySQLClient.getConnexion();
        this.utilisateur = null;

//        this.stage.setX(primaryScreenBounds.getMaxX()/6);
//        this.stage.setY(primaryScreenBounds.getMaxY()/4);

        this.stage.setWidth(primaryScreenBounds.getWidth()/1.3);
        this.stage.setHeight(primaryScreenBounds.getHeight()/2);
        this.stage.centerOnScreen();
        this.stage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }

}
