package client;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.regex.Pattern;

public class ActionCreer implements EventHandler<ActionEvent> {
    private Client app;
    public ActionCreer(Client app){
        this.app = app;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        String mail = app.getCreateMail();
        String pseudo = app.getCreatePseudo();
        String mdp = app.getCreateMdp();
        String confirmMdp = app.getCreateConfirmMdp();

        try{
            if(mdpValide(mdp, confirmMdp)) {
                if (mailValide(mail)) {
                    if(pseudoValide(pseudo)) {
                        ConnexionMySQL c = app.mySQL;
                        if(BibliMySQLClient.pseudoLibre(c, pseudo)) {
                            if (BibliMySQLClient.mailLibre(c, mail)) {
                                int idUtilisateur = BibliMySQLClient.getMaxID(c) + 1;
                                BibliMySQLClient.creerUtilisateur(c, idUtilisateur, pseudo, mail, mdp);

                                app.setUtilisateur(Utilisateur.instancierParID(c, idUtilisateur));
                                app.setStage(Nouveautes.setNouveautes(app), "Duel sur la toile - Nouveautés");
                            } else {
                                app.setCreateMessage("Adresse mail déjà utilisée");
                            }
                        } else {
                            app.setCreateMessage("Pseudonyme déjà utilisé");
                        }
                    } else {
                        app.setCreateMessage("Pseudonyme invalide (3 à 30\ncaractères alphanumériques)");
                    }
                } else {
                    app.setCreateMessage("Adresse mail invalide");
                }
            } else {
                app.setCreateMessage("Mots de passe invalides (8 à 30 caractères\nalphanumériques) ou différents");
            }
        } catch (NullPointerException | NoSuchAlgorithmException e) {
            System.out.println(e);
            app.setCreateMessage("Votre installation de Java\nn'est pas supportée");
        } catch (SQLException e) {
            System.out.println(e);
            app.setCreateMessage("Connexion au serveur impossible");
        }

    }

    private boolean pseudoValide(String pseudo){
        return Pattern.compile("\\w{3,30}").matcher(pseudo).matches();
    }

    private boolean mdpValide(String mdp, String confirm){
        return mdp.equals(confirm)&&Pattern.compile("\\w{8,30}").matcher(mdp).matches();
    }

    private boolean mailValide(String mail){
        return Pattern.compile("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
                Pattern.CASE_INSENSITIVE).matcher(mail).matches();
    }
}
