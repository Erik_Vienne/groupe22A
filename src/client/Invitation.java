package client;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Invitation {
    private int id;
    private Utilisateur env, dest;
    private java.util.Date date;
    private Jeu jeu;
    private boolean actif;
    private Invitation(ConnexionMySQL c, int id, int idEnv, int idDest, int idJeu, Date date, boolean actif) throws SQLException {
        this.id = id;
        this.env = Utilisateur.instancierParID(c, idEnv);
        this.dest = Utilisateur.instancierParID(c, idDest);
        this.jeu = Jeu.instancierParID(c, idJeu);
        this.actif = actif;
        this.date = date;
    }

    public static Invitation instancierParID(ConnexionMySQL c, int id) throws SQLException{
        if (!c.isConnecte())
            throw new SQLException();
        ResultSet rs = c.createStatement().executeQuery("select * from INVITATION where idInv="+id);
        rs.next();
        return new Invitation(c, id, rs.getInt("idEnv"), rs.getInt("idDest"), rs.getInt("idJeu"), rs.getDate("dateInv"), rs.getBoolean("activeInv"));
    }

    public int getId() {
        return id;
    }

    public boolean isActif() {
        return actif;
    }

    public java.util.Date getDate() {
        return date;
    }

    public Utilisateur getEnv() {
        return env;
    }

    public Utilisateur getDest() {
        return dest;
    }

    public Jeu getJeu() {
        return jeu;
    }
}

